/*
 * person.c
 *
 *  Created on: 2020. febr. 24.
 *      Author: marci
 */

#include <stdio.h>
#include <stdlib.h>
#include "person.h"

#define MALLOC(ptr,size) 	\
	do {					\
		ptr = malloc(size); \
		if(!ptr)			\
		abort();			\
	} while(0)

#define FREE(ptr)	\
	do {			\
		free(ptr);	\
		ptr = NULL; \
	} while(0)


static const int BUFFER_SIZE = 1024;

#define READ_MODE "r"

static struct PersonRepository {
	int initialized;
	TAILQ_HEAD(p_list, Person) head;
} person_repository = {.initialized = 0, .head = NULL};

int person_init(){
	TAILQ_INIT(&person_repository.head);
	person_repository.initialized = 1;
	return EXIT_SUCCESS;
}

int person_deinit(){
	struct Person *p;
	while((p=person_remove_head())){
		FREE(p->name);
		FREE(p->died);
		FREE(p);
	}
	return EXIT_SUCCESS;
}

static char *strchomp(char *line){
	if(!(line && *line))
		return line;

	char *begin = line;

	while(*line++)
		;

	while(--line >= begin){
		if(*line >= ' ')
			return begin;

		if(*line == '\r' || *line == '\n')
			*line = '\0';
	}
	return begin;
}

struct Person *person_create(char *name, int age, int state, char *died){
	if(!(name && *name))
		return NULL;

	struct Person *result = NULL;
	MALLOC(result, sizeof(struct Person));
	result -> name = name;
	result -> age = age;
	result -> state = state;
	result -> died = died;
	result -> next = NULL;

	return result;
}

static struct Person *person_process_file_line(char *line){
	if(!(line && *line))
		return NULL;

	char *n = strtok(line, ";");
	char *a = strtok(NULL, ";");
	char *s = strtok(NULL, ";");
	char *d = strtok(NULL, ";");

#ifdef STRICT
	if(n && *n && a && *a && s && *s && d && *d)
		return person_create(strdup(n),
							atoi(a),
							atoi(s),
							strdup(d));
#else
	if(n && *n)
		return person_create(strdup(n),
							a ? atoi(a) : 0,
							s ? atoi(s) : 0,
							d ? strdup(d) : "?");
#endif
	return NULL;
}

int person_load_from_file(const char* name){
	if(!(name && *name))
		return EXIT_FAILURE;

	FILE *f = fopen(name, READ_MODE);
	if(!f)
		return EXIT_FAILURE;

	char *line;
	MALLOC(line, BUFFER_SIZE);

	struct Person *person = NULL;

	while((fgets(line,BUFFER_SIZE,f))){
		person = person_process_file_line(strchomp(line));
		person_add(person);
	}
	FREE(line);
	fclose(f);
	return EXIT_SUCCESS;
}

int person_add(struct Person *person){
	if(!(person && person_repository.initialized))
		return EXIT_FAILURE;

	TAILQ_INSERT_TAIL(&person_repository.head, person, next);

	return EXIT_SUCCESS;
}

