/*
 * Copyright (C)  2016 Zoltan Zidarics (Zamek)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/** \file
    \brief
*/

#include <crc.h>

#define POLYNOMIAL 0xd8
#define POLYNOMIAL_MODBUS 0xa001

/**
    Calculate the crc value of a buffer
    \param data address of buffer
    \param d_len length of the buffer
    \return calculated crc
*/
crc16_t crc16_calc_buffer(char *data, size_t len) {
    crc16_t rmdr = 0;
    if (!data || len<=0)
        return 0;

    unsigned int i;
    for(i=0;i<len;i++)
        crc16_add_byte(&rmdr, *(data+i));

    return rmdr;
}

/** on the fly crc calculation
    add a byte to the crc
    \param crc aggregated value of crc
    \param byte to aggregate to crc
    \return new crc value
*/
void crc16_add_byte(crc16_t *crc, unsigned char b) {
    int bits;
    *crc ^= ((uint16_t)b)<<8;

    for (bits=8;bits>0;--bits)
        if (*crc & 0x8000) {
            *crc <<=1;
			*crc^=POLYNOMIAL;
        }
        else
            *crc <<= 1;
}

/** on the fly crc calculation for Modbus
    add a byte to the crc
    \param crc aggregated value of crc
    \param byte to aggregate to crc
    \return new crc value
*/
void crc_modbus_add_byte(crc16_t *crc, uint8_t b) {
    int i;
    *crc ^= (uint16_t)b;
    for (i=8;i>0;i--)
        if (*crc & 1) {
            *crc >>=1;
            *crc ^= POLYNOMIAL_MODBUS;
        }
        else
            *crc >>= 1;
}

/**
    Crc begin for different methods
    \param address of crc value
*/
void crc_modbus_begin(crc16_t *crc) {
	*crc = (crc16_t) 0xffff;
}

void crc16_begin(crc16_t *crc) {
	*crc=0;
}

