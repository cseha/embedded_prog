#Commonly used make rules

flash:$(BIN_DIR)/$(PROJECT).elf
	st-flash write $(BIN_DIR)/$(PROJECT).bin 0x8000000
	sleep 2
	gtkterm -c $(BOARD)

doc:
	$(MAKE) -C doc

.PHONY: flash doc
