/*
 * statistics.c
 *
 *  Created on: 1 Nov 2020
 *      Author: mark
 */

#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "queue.h"
#include "string.h"
#include "shell.h"
#include "math.h"

#include "statistics.h"

#define LIST_LIMIT (250)

queue_head_t List = {NULL, 0}; //list handler to save data, had to do here, cmd function uses it

void statistics_task(){

	bounces_stat_t bounces; //struct to save data from xQueue

	for(;;){
		xQueueReceive(bounces_queue, &bounces.data, portMAX_DELAY); //writes incoming data to bounces

		addBounceToList(&List, bounces.data); //saving the data to List
		pr_notice("data received | ID: %d\t | list size: %d\r\n", bounces.data.ID, List.size);

		osDelay(100);
	}
}


/**
 * data is saved with this function
 * \param: pointer to list handler
 * \param: data from xQueue
 * once the list size reaches LIST_LIMIT the first data should be erased DOES'NT WORK RN
 */
void addBounceToList(queue_head_t *list, bounces_t data){
	if(!list){
		sh_printf("THERE IS NO HEAD\r\n");
		return;
	}
	bounces_stat_t* tmp = (bounces_stat_t*) malloc(sizeof(bounces_stat_t));
    tmp->data = data;
    tmp->next = NULL;
    tmp->prev = NULL;

	if(list->head == NULL){
    	list->head = tmp;
    }else{
    	bounces_stat_t *EOL =  list->head;

    	while(EOL->next)
    	{
    		EOL = EOL->next;
    	}

    	EOL->next = tmp;
    	tmp->prev = EOL;
    }
/*
	if(list->size == LIST_LIMIT-1){
		bounces_stat_t *new_head = list->head->next;
		free(list->head);
		list->head = new_head;
	}
*/
    list->size++;
}

/**
 * calculates the average of the collected data
 *\param: pointer to list-handler
 *\retval: result
 */
float getBounceMean(queue_head_t *list){
	if(!list->head){
		return 0;
	}

    bounces_stat_t* tmp = list->head;
    uint32_t sum = 0;

    while(tmp){
        sum += tmp->data.count;
        tmp = tmp->next;
    }
    return (float)sum / (float)list->size;
}

/**
 * calculates the variance of the collected data
 *\param: result of getBounceAverage();
 *\retval: result
 */
float getBounceVariance(float mean, queue_head_t *list){
	if(!list->head){
		return 0;
	}
	bounces_stat_t* tmp = list->head;
	uint32_t sumdiffsquare = 0;
	uint32_t sumdiff = 0;

	while(tmp){
		sumdiff = tmp->data.count - mean;
		sumdiffsquare += pow(sumdiff, 2);
        tmp = tmp->next;
    }

	float variance = (float)sumdiffsquare / (float)(list->size);

    return variance;
}

/**
 * calculates the deviation of the collected data
 *\param: result of getBounceAverage();
 *\retval: result
 */
float getBounceDeviation(float variance){
    return (float)sqrt(getBounceVariance(variance, &List));
}

/**
 * prints all collected data
 *\param: pointer to list handler
 */
void printBounces(queue_head_t *list){
    if(!list->head){
        sh_printf("no data\r\n");
        return;
    }

    bounces_stat_t *tmp = list->head;

    while(tmp){
        sh_printf("id: %d\t | bounces: %d\t | timer: %dms | switch was %s\r\n",
        tmp->data.ID, tmp->data.count, tmp->data.sw_time_ms, tmp->data.state ? "on" : "off");
        tmp = tmp->next;
    }
}

/**
 * returns last received data from xQueue
 * \param: pointer to list handler
 */
void printLastBounce(queue_head_t *list){
	if(!list->head){
		sh_printf("no data\r\n");
		return;
	}

    bounces_stat_t *tmp = list->head;
    while(tmp->next){
    	tmp = tmp->next;
    }

    sh_printf("last bounce: %d\r\n", tmp->data.count);
}

/**
 * prints last state time
 * \param: pointer to list handler
 * \param: state of switch
 */
void printLastSwitchTime(queue_head_t *list, SW_STATE state){
	if(!list->head){
		sh_printf("no data\r\n");
		return;
	}

	bounces_stat_t *tmp = list->head;

	while(tmp->next){
		tmp = tmp->next;
	}

	if(state){
		sh_printf("last ON time: %dms\r\n", tmp->data.sw_time_ms);
		return;
	}

	bounces_stat_t *tmp2 = tmp->prev;
	sh_printf("last OFF time: %dms\r\n", tmp2->data.sw_time_ms);
}

/**
 * erases the list and frees the memory
 * \param: pointer to list handler
 */
void eraseBouncesList(queue_head_t *list){
	if(!list->head){
		sh_printf("no data\r\n");
		return;
	}

    bounces_stat_t* tmp =  list->head;

    while(list->head){
        tmp = tmp->next;
        free(list->head);
        list->head = tmp;
        list->size--;
    }

    sh_printf("data erased\r\n");
}

/**
 * CONSOLE STUFF
 */
void stat_cmd_usage(){
	sh_printf("usage of %s\r\n\t"
			"> printall\t(print all data)\r\n\t"
			"> mean\t\t(print the mean of the data set)\r\n\t"
			"> var\t\t(print the variance of the data set)\r\n\t"
			"> dev\t\t(print the standard deviation of the data set)\r\n\t"
			"> lastbounce\t(print last bounce)\r\n\t"
			"> laston\t(print last ON time)\r\n\t"
			"> lastoff\t(print last OFF time)\r\n\t"
			"> erase\t\t(erase all data)\r\n",
			SHELL_COMMAND_STATISTICS);
}

#define CMD_ARG_PRINT_ALL_DATA "printall"
#define CMD_ARG_MEAN "mean"
#define CMD_ARG_VARIANCE "var"
#define CMD_ARG_DEVIATION "dev"
#define CMD_ARG_LAST_BOUNCE "lastbounce"
#define CMD_ARG_LAST_ON "laston"
#define CMD_ARG_LAST_OFF "lastoff"
#define CMD_ARG_ERASE "erase"

void cmd_statistics(int argc, char **argv){
	if(argc == 0){
		stat_cmd_usage();
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_PRINT_ALL_DATA) == 0){
		printBounces(&List);
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_MEAN) == 0){
		sh_printf("mean is %.2f\r\n",
				getBounceMean(&List));
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_VARIANCE) == 0){
		sh_printf("variance is %.2f\r\n",
				getBounceVariance(getBounceMean(&List), &List));
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_DEVIATION) == 0){
		sh_printf("standard deviation is %.2f\r\n",
				getBounceDeviation(getBounceVariance(getBounceMean(&List), &List)));
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_LAST_BOUNCE) == 0){
		printLastBounce(&List);
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_LAST_ON) == 0){
		printLastSwitchTime(&List, ON);
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_LAST_OFF) == 0){
		printLastSwitchTime(&List, OFF);
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_ERASE) == 0){
		eraseBouncesList(&List);
		return;
	}

stat_cmd_usage();
}

