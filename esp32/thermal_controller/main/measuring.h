/*
 * analog.h
 *
 *  Created on: Nov 17, 2020
 *      Author: mark
 */

#ifndef MAIN_MEASURING_H_
#define MAIN_MEASURING_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define MAX_TEMPERATURE (100)
#define MIN_TEMPERATURE (0)
#define MAX_DIGITAL (4096)
#define MIN_DIGITAL (0)
#define CHANNELS (2) //increment this when adding new channel
#define BOYLER_CHANNEL (0)
#define FLOOR_CHANNEL (1)

void analog_init();
BaseType_t flash_init();
uint16_t analog_get(uint8_t channel, BaseType_t wait_tick);
double  temperature_get(uint8_t channel, uint16_t dig_input);



#endif /* MAIN_MEASURING_H_ */
