/*
 * button.h
 *
 *  Created on: 31 Oct 2020
 *      Author: mark
 */

#ifndef INC_BUTTON_H_
#define INC_BUTTON_H_

#include "FreeRTOS.h"
#include "stdbool.h"
#include "queue.h"

QueueHandle_t bounces_queue;

typedef enum {
	ON = 1,
	OFF = 0
}SW_STATE;

typedef struct {
	uint8_t ID;
	uint32_t sw_time_ms;
	uint8_t count;
	SW_STATE state;
}bounces_t;

void button_task();

bounces_t *bounce_counter(bounces_t *bounces, uint8_t counter, uint32_t timer);

#endif /* INC_BUTTON_H_ */
