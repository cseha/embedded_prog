/*
 * controller.h
 *
 *  Created on: Dec 18, 2020
 *      Author: mark
 */

#ifndef MAIN_CONTROLLER_H_
#define MAIN_CONTROLLER_H_

#include "freertos/semphr.h"

#define SWITCH_WAIT_TIME_SEC (60) //1 min

void controller_init();


#endif /* MAIN_CONTROLLER_H_ */
