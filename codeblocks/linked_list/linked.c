#include <string.h>
#include <sys/queue.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "linked.h"

bounces_stat_t *createBounceListWithFirstData(queue_head_t *list, bounces_t data){
    bounces_stat_t* temp = (bounces_stat_t*) malloc(sizeof(bounces_stat_t));
    temp->data = data;
    temp->next = NULL;

    list->head = temp;
    list->size = 1;

    return temp;
}
void addBounceToList(queue_head_t *list, bounces_t data){
    bounces_stat_t* temp = (bounces_stat_t*) malloc(sizeof(bounces_stat_t));
    temp->data = data;
    temp->next = NULL;

	if(list->head == NULL){
    	list->head = temp;
    }else{
    	bounces_stat_t *EOL =  list->head;

    	while(EOL->next)
    	{
    		EOL = EOL->next;
    	}

    	EOL->next = temp;
    }

    list->size++;
}

int getBounceByID(int ID){
    return 1;
}

float getBounceMean(queue_head_t *list){
    bounces_stat_t* EOL = list->head;
    int sum = 0;

    while(EOL->next){
        sum += EOL->data.count;
        EOL = EOL->next;
    }
    return (float)sum / (float)list->size;
}

float getBounceVariance(int deviation){

    return 1;
}

float getBounceDeviation(int mean, queue_head_t *list){

}

void printBounces(queue_head_t *list){
    if(!list->head){
        printf("no data to print\n");
    }
    bounces_stat_t *tmp = list->head;

    while(tmp){
        printf("id: %d | bounces: %d | direction %d\n", tmp->data.ID, tmp->data.count, tmp->data.direction);
        tmp = tmp->next;
    }
}


void eraseBouncesList (queue_head_t *list)
{
    bounces_stat_t* temp =  list->head;

    while(list->head)
    {
        temp = temp->next;
        free(list->head);
        list->head = temp;
        list->size--;
    }
}

int getLastBounce(queue_head_t *list){
    bounces_stat_t *EOL = list->head;
    while(EOL->next){
        EOL = EOL->next;
    }
    return EOL->data.count;
}
