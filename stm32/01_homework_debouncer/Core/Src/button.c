/*
 * button.c
 *
 *  Created on: 31 Oct 2020
 *      Author: mark
 */

#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "main.h"
#include "string.h"
#include "queue.h"
#include "shell.h"
#include "button.h"

#define TASK_DELAY_MS (1)
#define PRELLING_TIME_MS (5)
#define BOUNCES_QUEUE_LENGTH (1)
#define BOUNCES_QUEUE_ITEM_SIZE sizeof(bounces_t)

void button_task(){
	bounces_queue = xQueueCreate(BOUNCES_QUEUE_LENGTH, BOUNCES_QUEUE_ITEM_SIZE);

	uint8_t previous_state = 1;
	bounces_t bounces = {0, 0 , 0, ON};
	uint32_t timer = 0;
	uint8_t counter = 0;
	uint8_t quite_mode = 1;
	uint8_t prelling = PRELLING_TIME_MS;

	for(;;){
		uint8_t new_state = HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin);

		if(new_state == previous_state){
			if(quite_mode == 0){
				if(prelling == 0){
					HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, !new_state);
					xQueueSend(bounces_queue, bounce_counter(&bounces, counter, timer), (TickType_t) 0);
					prelling = PRELLING_TIME_MS;
					counter = 0;
					timer = 0;
					quite_mode = 1;
				}
				else
					prelling--;
			}else
				timer++;
		}else{
			quite_mode = 0;
			counter++;
			prelling = PRELLING_TIME_MS;
		}

		previous_state = new_state;
		vTaskDelay(TASK_DELAY_MS);
	}
}
/**
 * writes counter value to struct
 * \param: pointer to struct
 * \param: counter value
 */
bounces_t *bounce_counter(bounces_t *bounces, uint8_t counter, uint32_t timer){
	bounces->sw_time_ms = timer;
	bounces->count = counter;
	bounces->ID = bounces->ID +1;
	bounces->state = !bounces->state;

	return bounces;
}
