/**
 * \file charqueue.c
 *
 * Created Nov 5, 2016
 *
 * \author zamek
*/

#include <charqueue.h>


#undef USE_MUTEX

#if defined(USE_MUTEX)
static osMutexId mutex;
osMutexDef(mutex);
#endif

BaseType_t cq_init(char_queue_t *queue, uint16_t size) {
	if(! (queue && size > 0))
		return pdFAIL;

	queue->buffer=pvPortMalloc(size);
	queue->r_ptr=0;
	queue->w_ptr=0;
	queue->size=size;
#if defined(USE_MUTEX)
	mutex = osMutexCreate(osMutex(mutex));
#endif
	return pdTRUE;
}

uint8_t cq_is_full(char_queue_t *queue) {
	return queue ? queue->cnt >= queue->size : 1;
}

BaseType_t cq_put(char_queue_t *queue, uint8_t c) {
	if(!queue)
		return pdFAIL;

#if defined(USE_MUTEX)
	osMutexWait(mutex, osWaitForever);
#else
	portDISABLE_INTERRUPTS();
#endif

	*(queue->buffer + queue->w_ptr) = c;
	queue->w_ptr = (queue->w_ptr + 1) % queue->size;
	++queue->cnt;

#if defined(USE_MUTEX)
	osMutexRelease(mutex);
#else
	portENABLE_INTERRUPTS();
#endif
	return pdTRUE;
}

BaseType_t cq_get(char_queue_t *queue, uint8_t *c) {
	if(! (queue && c && queue->cnt > 0))
		return pdFAIL;

#if defined(USE_MUTEX)
	osMutexWait(mutex, osWaitForever);
#else
	portDISABLE_INTERRUPTS();
#endif
	*c=*(queue->buffer+queue->r_ptr);
	queue->r_ptr=(queue->r_ptr+1)%queue->size;
	--queue->cnt;

#if defined(USE_MUTEX)
	osMutexRelease(mutex);
#else
	portENABLE_INTERRUPTS();
#endif

	return pdTRUE;
}
