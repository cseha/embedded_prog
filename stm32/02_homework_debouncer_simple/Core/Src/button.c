/*
 * button.c
 *
 *  Created on: 31 Oct 2020
 *      Author: mark
 */

#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "main.h"
#include "string.h"
#include "shell.h"
#include "button.h"

#define TASK_DELAY_MS (1)
#define PRELLING_TIME_MS (50)
#define bounces_t_QUEUE_LENGTH (1)
#define bounces_t_QUEUE_ITEM_SIZE sizeof(bounces_t_t)

typedef enum {
	ON = 1,
	OFF = 0
}SW_STATE;

typedef struct {
	uint32_t sw_time_laston ;
	uint32_t sw_time_lastoff;
	uint8_t last_bounce;
	uint32_t total_bounces;
	float mean;
	uint16_t sws ;
	SW_STATE state ;
}bounces_t;

static bounces_t bounces = {0, 0, 0, 0, 0, 0, ON};

void writeToBounces(bounces_t *bounces, uint8_t counter, uint32_t timer);

void button_task(){

	uint8_t previous_state = 1;
	uint32_t timer = 0;
	uint8_t counter = 0;
	uint8_t quite_mode = 1;
	uint8_t prelling = PRELLING_TIME_MS;

	for(;;){
		uint8_t new_state = HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin);

		if(new_state == previous_state){
			if(quite_mode == 0){
				if(prelling == 0){
					HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, !new_state);
					writeToBounces(&bounces, counter, timer);
					prelling = PRELLING_TIME_MS;
					counter = 0;
					timer = 0;
					quite_mode = 1;
				}
				else
					prelling--;
			}else
				timer++;
		}else{
			counter++;
			quite_mode = 0;
			prelling = PRELLING_TIME_MS;
		}

		previous_state = new_state;
		vTaskDelay(TASK_DELAY_MS);
	}
}

void writeToBounces(bounces_t *bounces, uint8_t counter, uint32_t timer){
	bounces->sws++;
	bounces->total_bounces = bounces->total_bounces + counter;
	bounces->last_bounce = counter;
	bounces->mean = (float)(bounces->total_bounces)/(float)bounces->sws;

	if(bounces->state)
		bounces->sw_time_lastoff = timer;
	else
		bounces->sw_time_laston = timer;

	bounces->state = !bounces->state;
}

/**
 * CONSOLE STUFF
 */
void stat_cmd_usage(){
	sh_printf("usage of %s\r\n\t> mean\t\t(prints the mean of total bounces since app is running)\r\n\t"
			"> lastbounce\t(print last bounce)\r\n\t"
			"> laston\t(print last ON time)\r\n\t"
			"> lastoff\t(print last OFF time)\r\n",SHELL_COMMAND_STATISTICS);
}

#define CMD_ARG_MEAN "mean"
#define CMD_ARG_LAST_BOUNCE "lastbounce"
#define CMD_ARG_LAST_ON "laston"
#define CMD_ARG_LAST_OFF "lastoff"

void cmd_statistics(int argc, char **argv){
	if(argc == 0){
		stat_cmd_usage();
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_MEAN) == 0){
		sh_printf("mean: %.2f\r\n", bounces.mean);
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_LAST_BOUNCE) == 0){
		sh_printf("last bounce: %d, total bounces: %d, total switches: %d\r\n",
				bounces.last_bounce, bounces.total_bounces, bounces.sws);
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_LAST_ON) == 0){
		sh_printf("last ON time: %dms\r\n", bounces.sw_time_laston);
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_LAST_OFF) == 0){
		sh_printf("last OFF time: %dms\r\n", bounces.sw_time_lastoff);
		return;
	}

stat_cmd_usage();
}
