/*
 * statistics.h
 *
 *  Created on: 1 Nov 2020
 *      Author: mark
 */

#ifndef INC_STATISTICS_H_
#define INC_STATISTICS_H_

#include "FreeRTOS.h"
#include "button.h"

#define SHELL_COMMAND_STATISTICS "stat"

#define STATISTICS_COMMANDS \
{ .command_name = SHELL_COMMAND_STATISTICS, .function = cmd_statistics}

typedef struct {
	bounces_t data;
	struct bouncer_stat_t *prev;
	struct bouncer_stat_t *next;
} bounces_stat_t;

typedef struct {
    bounces_stat_t *head;
    int size;
} queue_head_t;

void statistics_task();

bounces_stat_t *createBounceListWithFirstData(queue_head_t *list, bounces_t data);

void addBounceToList(queue_head_t *list, bounces_t data);

void printLastBounce(queue_head_t *list);

float getBounceTimeMean(queue_head_t *list);

float getBounceTimeVariance(float mean, queue_head_t *list);

float getBounceTimeDeviation(float variance);

void printBounces(queue_head_t *head);

void eraseBouncesList(queue_head_t *list);

void printLastSwitchTime(queue_head_t *list, SW_STATE state);

void cmd_statistics(int argc, char **argv);

#endif /* INC_STATISTICS_H_ */
