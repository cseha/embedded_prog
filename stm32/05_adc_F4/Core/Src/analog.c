/*
 * analog.c
 *
 *  Created on: Oct 20, 2020
 *      Author: mark
 */

#include "shell.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "string.h"
#include "cmsis_os.h"

#include "analog.h"

#define AD_MAX_VAL (4096)
#define AD_MIN_VAL (1500)

#define NOTIFY_WAIT_MS (100)
#define CONV_INTERVAL_MS (20)
#define TASK_AD_SLEEP_TIME_MS (100)

#define EVT_AD_CONV_END (1>>0)
#define EVT_AD_CONV_START (1>>1)
#define EVT_AD_CONV_STOP (1>>2)
#define EVT_AD_MASK ((EVT_AD_CONV_START) | (EVT_AD_CONV_STOP) | (EVT_AD_CONV_END))

typedef struct {
	uint32_t ad_start_error;
	uint32_t ad_stop_error;
	uint32_t ad_conv_success;
	uint32_t ad_conv_error;
}ad_statistics_t;

typedef struct{
	volatile uint8_t ad_running;
	uint32_t ad_raw_value[ADC_ALL_CHANNELS];
	uint32_t ad_current_value[ADC_ALL_CHANNELS];
	ad_statistics_t ad_statistics;
}ad_task_control_t;

static ad_task_control_t ad_task_control;
extern ADC_HandleTypeDef hadc1;
extern osThreadId_t ADTaskHandle;

BaseType_t ad_init(){
	bzero(&ad_task_control.ad_raw_value, sizeof(ad_task_control.ad_raw_value));
	bzero(&ad_task_control.ad_current_value, sizeof(ad_task_control.ad_current_value));
	bzero(&ad_task_control.ad_statistics, sizeof(ad_task_control.ad_statistics));
	return pdPASS;
}

BaseType_t ad_deinit(){
	return pdPASS;
}

static void start_ad_conversion(){
	BaseType_t result = HAL_ADC_Start_DMA(&hadc1, &ad_task_control.ad_raw_value[0], AD_CHANNELS);

	if(result != HAL_OK)
		++ad_task_control.ad_statistics.ad_start_error;
	else
		ad_task_control.ad_running = 1;

}

//IT
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	++ad_task_control.ad_statistics.ad_conv_success;
	BaseType_t hptw = pdFALSE;
	xTaskNotifyFromISR(ADTaskHandle, EVT_AD_CONV_END, eSetBits, &hptw);
	portYIELD_FROM_ISR(hptw);

}

//IT
void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc){
	++ad_task_control.ad_statistics.ad_conv_error;
	HAL_ADC_Stop_DMA(&hadc1);
}

void ad_processing_task(void const *argument){
	uint32_t notified_value;
	BaseType_t result;

	for(;;){
		result = xTaskNotifyWait(EVT_AD_MASK, EVT_AD_MASK, &notified_value, pdMS_TO_TICKS(NOTIFY_WAIT_MS));

		if(notified_value & EVT_AD_CONV_STOP){
			ad_task_control.ad_running = 0;
			result = HAL_ADC_Stop_DMA(&hadc1);

			if(result != HAL_OK){
				++ad_task_control.ad_statistics.ad_stop_error;
				pr_err("AD stop error");
			}
		}

		if(notified_value & EVT_AD_CONV_START){
			start_ad_conversion();
		}

		if(notified_value & EVT_AD_CONV_END){
			//xSemaphorTake();
			memcpy(&ad_task_control.ad_current_value, &ad_task_control.ad_raw_value, sizeof(ad_task_control.ad_current_value));
			++ad_task_control.ad_statistics.ad_conv_success;
			osDelay(CONV_INTERVAL_MS);

			if(!ad_task_control.ad_running)
				continue;

			start_ad_conversion();
			continue;
		}
	}
}

#define CMD_HELP "help"
#define CMD_STATS "stats"
#define CMD_STOP "stop"
#define CMD_START "start"

static void cmd_usage()
{
	sh_printf("%s usage: [ help | start | stop | stats | vis < start | stop > ]\r\n", TASK_COMMAND_AD);
}

void cmd_analog(int argc, char **argv){
	if(argc <= 0 || strcasecmp(argv[0], CMD_HELP) == 0)
	{
		cmd_usage();
		return;
	}

	if(strcasecmp(argv[0], CMD_STATS) == 0)
	{
		sh_printf("state is %s\r\n", ad_task_control.ad_running ? "started" : "stopped");
		sh_printf("converions: %d | conversion errors: %d | start errors: %d | stop errors: %d\r\n",
					ad_task_control.ad_statistics.ad_conv_success,
					ad_task_control.ad_statistics.ad_conv_error,
					ad_task_control.ad_statistics.ad_start_error,
					ad_task_control.ad_statistics.ad_stop_error);
		return;
	}

	if(strcasecmp(argv[0], CMD_START) == 0)
	{
		if(ad_task_control.ad_running)
		{
			sh_printf("AD task is already running\r\n");
			return;
		}

		xTaskNotify(ADTaskHandle, EVT_AD_CONV_START, eSetBits);
		sh_printf("AD task is starting\r\n");
		return;
	}

	if(strcasecmp(argv[0], CMD_STOP) == 0)
	{
		if(!ad_task_control.ad_running)
		{
			sh_printf("AD task is already stopped\r\n");
			return;
		}

		xTaskNotify(ADTaskHandle, EVT_AD_CONV_STOP, eSetBits);
		sh_printf("AD task is stopping\r\n");
		return;
	}

	cmd_usage();
}






