/*
 * counter.c
 *
 *  Created on: Oct 9, 2020
 *      Author: mark
 */

#include <stdbool.h>
#include <string.h>

#include "cmsis_os.h"
#include "shell.h"
#include "counter.h"

#define MIN_VAL 50
#define MAX_VAL 500
#define CMD_ARG_SET "set"
#define CMD_ARG_GET "get"
#define TASK_DELAY_MS 1000

static struct{
	bool initialised;
	uint16_t value;
} counter_control;



BaseType_t counter_init(){
	counter_control.initialised = true;
	counter_control.value = MIN_VAL;
	return pdTRUE;
}

BaseType_t counter_deinit(){
	return pdTRUE;
}

void counter_task(const void *args){
	for(;;){
		counter_control.value = counter_control.value <= MAX_VAL
			? counter_control.value + 1
			: MIN_VAL;

		osDelay(TASK_DELAY_MS);
	}
}

static void cmd_usage(){
	sh_printf("usage: %s [set <value> | get]\r\n", COUNTER_CMD);
}
void cmd_counter(int argc, char **argv){
	if(!counter_control.initialised){
		sh_printf("counter is not initialised\r\n");
		return;
	}

	if(argc == 0){
		cmd_usage();
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_SET) == 0){
		if(argc != 2){
			cmd_usage();
			return;
		}

		uint16_t nv = atoi(argv[1]);
		if(nv >= MIN_VAL && nv <= MAX_VAL){
			counter_control.value = nv;
			sh_printf("value set to %d\r\n", nv);
			return;
		}

		sh_printf("value must be between %d and %d\r\n", MIN_VAL, MAX_VAL);
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_GET) == 0){
		sh_printf("value is %d\r\n", counter_control.value);
		return;
	}

	sh_printf("unknown command: %s\r\n", argv[0]);
	cmd_usage();
}

