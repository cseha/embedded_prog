/**
 * \file shell.h
 *
 * Created Oct 30, 2016
 *
 * \author zamek
 * \author Giovanni Di Sirio
*/

#ifndef SHELL_H_
#define SHELL_H_

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <FreeRTOS.h>
#include <stm32f3xx_hal.h>
#include <stdarg.h>

/**
 * \defgroup SHELL
 * @{
 */

/**
 * \addtogroup SHELL
 *
 * Shell and log module for STM32.
 *
 * This module can communicate through a serial port as a logger and shell.
 * You can connect to the port with a simple terminal emulator like gtkterm, putty or other simple terminal.
 * Default behaviour is a logger with pr_xxx macros. If you want to use log message with data and time, you can set \link DATE_IN_LOG_MESSAGES, \endlink
 * but it needs RTC on the system.
 * Console can use history (with up and down arrow keys) and completion facility (with TAB key) if you want. You have to set \link SHELL_USE_HISTORY \endlink to 1 and \link SHELL_USE_COMPLETION \endlink to 1.
 *
 * You can use the serial port as a console. You can switch on/off the logging facility with \a \b log \a \b on / \a \b log \a \b off command in the console.
 * There are a lot of predefined commands and every module can define own commands. You can get help with \a \b help command.
 * There are to level of users: normal and root user. Root has higher level than normal user. You can change your level with \a \b su or \a \b logout command.
 *
 * The default commands are:
 * <ul>
 * <li>\a \b su change to super user. You have to know the super user password.</li>
 * <li>\a \b logout Logout from super user mode.</li>
 * <li>\a \b dump &lt;address as hex&gt; [length] Dump a memory from address with length. Address is hexadecimal and length can be decimal or hexadecimal.
 * The default value of length is 10. <br/>
 * \b Warning! There are addresses in stm32, which are cannot be readable. Using this addresses causes segfault.</li>
 * <li>\a\b log [on/off] Get the log status or set to on/of logging.</li>
 * </ul>
 *
 * Command definition
 *
 * You can define on command with the following structure: \link shell_command_t \endlink
 *
 * For example if you have two commands as comm1 and comm2, you can define it like this:
 *
 * \code
 *
 *	#define MY_COMMANDS
 *	{ .command_name="comm1",    .function=cmd_comm1 							},
 *	{ .command_name="comm2",    .function=cmd_comm2 	.only_for_root=1 		}
 *
 * \endcode
 *
 * Function prototype is \link shell_cmd_t. \endlink
 *
 * If you set only_for_root not 0 the commands will be root only executable.
 *
 * You can make a global command definition like this:
 * \code
 *
 * shell_command_t shell_commands[] ={
 * MY_COMMANDS,
 *	{NULL, NULL}
 * };
 *
 * \endcode
 *
 * The shell configrutaion is:
 * \code
 *
 * shell_configuration_t shell_config= {
 *	.uart=&huart2,
 *	.commands=shell_commands,
 *	.stack_size=1024,
 *	.priority=osPriorityNormal,
 *	.rtc=NULL,
 *	.history_buffer_size=128
 * };
 *
 * \endcode
 *
 * <ul>
 * <li>\b uart is the serial port \link UART_HandleTypeDef \endlink </li>
 * <li>\b commands is the chell_commands array</li>
 * <li>\b stack_size is the size of the shell task stack</li>
 * <li>\b priority is the shell task priority</li>
 * <li>\b rtc the rtc address if you want date tagging in logging</li>
 * <li>\b history_buffer the size of the history buffer if you want to use history service. If you want to user history, you have to set \link SHELL_USE_HISTORY \endlink to 1</li>
 * </ul>
 *
 * Initializing shell in the main after HAL_Init:
 * \code
 *
 *   sh_init(&shell_config);
 *
 * \endcode
 *
 * @{
 */
#ifdef __cplusplus
extern "Shell" {
#endif

/**
 * use floating point format in sh_printf
 */
#define STPRINTF_USE_FLOAT

#if defined(DEBUG)
#define SHELL_DEBUG
#else
#undef SHELL_DEBUG
#endif

/**
 * Maximum length of shell input line
 */
#define SHELL_MAX_LINE_LENGTH 64

/**
 * Maximum number of arguments of a command
 */
#define SHELL_MAX_ARGUMENTS 5

/**
 * super user password
 */
#if !defined(SUPER_USER_PASSWORD)
#define SUPER_USER_PASSWORD "secret"
#endif

/**
 * Application title
 */
#define APP_TITLE "=== STM32 Real time operating system ==="

/**
 * Date is in log messages
 */
#undef DATE_IN_LOG_MESSAGES

/**
 * Use standard printf. It is very ugly and uses much more resources
 */
#undef USE_STANDARD_PRINTF

/**
 * shell has history facility
 */
#define SHELL_USE_HISTORY 1

/**
 * shell has completion facility
 */
#define SHELL_USE_COMPLETION 1

/**
 * New line string
 */
#define SH_NL_STR "\r\n"

/**
 * shell can handle esc sequences
 */
#define SHELL_USE_ESC_SEQ 1

/**
 *  Dimensions a buffer to be used by the UART driver, if the UART driver uses a buffer at all.
 * */
#define SHELL_QUEUE_LENGTH                 255

/**
 * length of transmit queue
 */
#define TRANSMIT_QUEUE_LENGTH			   1024

/**
 * Waiting for the transmit mutex
 */
#define SHELL_MAX_MUTEX_WAIT               pdMS_TO_TICKS( 300 )

/**
 * Log tag for emergency messages
 */
#define CONSOLE_EMERG        "[Emerg ]: "

/**
 * Log tag for alert messages
 */
#define CONSOLE_ALERT        "[Alert ]: "

/**
 * Log tag for critical messages
 */
#define CONSOLE_CRIT         "[Crit  ]: "

/**
 * Log tag for error messages
 */
#define CONSOLE_ERR          "[Error ]: "

/**
 * Log tag for warning messages
 */
#define CONSOLE_WARN         "[Warn  ]: "

/**
 * Log tag for notice messages
 */
#define CONSOLE_NOTICE       "[Notice]: "

/**
 * Log tag for info messages
 */
#define CONSOLE_INFO         "[Info  ]: "

/**
 * Log tag for debug messages
 */
#define CONSOLE_DEBUG        "[Debug ]: "

/**
 * Length of data string in messages
 */
#define DATE_TIME_LENGTH 18

#if defined(DATE_IN_LOG_MESSAGES)
#define LOG_DATE sh_log_get_date_and_time()
#else
#define LOG_DATE
#endif
/*
 * These can be used to print at the various log levels.
 * All of these will print unconditionally, although note that pr_debug()
 * and other debug macros are compiled out unless either DEBUG is defined
 * or CONFIG_DYNAMIC_DEBUG is set.
 */
#if USE_STANDARD_PRINTF
#define pr_emerg(fmt, ...) 							\
	do {											\
		LOG_DATE;									\
		printf(CONSOLE_EMERG);						\
        printf(fmt, ##__VA_ARGS__);					\
		printf(SH_NL_STR);							\
	} while(0)

#define pr_alert(fmt, ...) 							\
		do {										\
			LOG_DATE;								\
			printf(CONSOLE_ALERT);					\
			printf(fmt, ##__VA_ARGS__);				\
			printf(SH_NL_STR);						\
		}while(0)

#define pr_crit(fmt, ...)  							\
		do {										\
			LOG_DATE;								\
			printf(CONSOLE_CRIT);					\
			printf(fmt, ##__VA_ARGS__);				\
			printf(SH_NL_STR);						\
		}while(0)

#define pr_err(fmt, ...)  							\
		do {										\
			LOG_DATE;								\
			printf(CONSOLE_ERR);					\
			printf(fmt, ##__VA_ARGS__);				\
			printf(SH_NL_STR);						\
		}while(0)

#define pr_warning(fmt, ...)  						\
		do {										\
			LOG_DATE;								\
			printf(CONSOLE_WARN);					\
			printf(fmt, ##__VA_ARGS__);				\
			printf(SH_NL_STR);						\
		}while(0)

#define pr_warn pr_warning

#define pr_notice(fmt, ...)  						\
		do {										\
			LOG_DATE;								\
			printf(CONSOLE_NOTICE);					\
			printf(fmt, ##__VA_ARGS__);				\
			printf(SH_NL_STR);						\
		}while(0)

#define pr_info(fmt, ...)  							\
		do {										\
			LOG_DATE;								\
			printf(CONSOLE_INFO);					\
			printf(fmt, ##__VA_ARGS__);				\
			printf(SH_NL_STR);						\
		}while(0)

#define pr_debug(fmt, ...)  						\
		do {										\
			LOG_DATE;								\
			printf(CONSOLE_DEBUG);					\
			printf(fmt, ##__VA_ARGS__);				\
			printf(SH_NL_STR);						\
		}while(0)

#else
/**
 * print emergency log message
 */
#define pr_emerg(fmt, ...) 							\
	do {											\
		LOG_DATE;									\
		sh_log_printf(CONSOLE_EMERG);				\
        sh_log_printf(fmt, ##__VA_ARGS__);			\
		sh_log_printf(SH_NL_STR);					\
	} while(0)

/**
 * print alert log message
 */
#define pr_alert(fmt, ...) 							\
		do {										\
			LOG_DATE;								\
			sh_log_printf(CONSOLE_ALERT);			\
			sh_log_printf(fmt, ##__VA_ARGS__);		\
			sh_log_printf(SH_NL_STR);				\
		}while(0)

/**
 * print critcial log message
 */
#define pr_crit(fmt, ...)  							\
		do {										\
			LOG_DATE;								\
			sh_log_printf(CONSOLE_CRIT);			\
			sh_log_printf(fmt, ##__VA_ARGS__);		\
			sh_log_printf(SH_NL_STR);				\
		}while(0)

/**
 * print error log message
 */
#define pr_err(fmt, ...)  							\
		do {										\
			LOG_DATE;								\
			sh_log_printf(CONSOLE_ERR);				\
			sh_log_printf(fmt, ##__VA_ARGS__);		\
			sh_log_printf(SH_NL_STR);				\
		}while(0)


/**
 * print warning log message
 */
#define pr_warning(fmt, ...)  						\
		do {										\
			LOG_DATE;								\
			sh_log_printf(CONSOLE_WARN);			\
			sh_log_printf(fmt, ##__VA_ARGS__);		\
			sh_log_printf(SH_NL_STR);				\
		}while(0)

/**
 * print warn log message
 */
#define pr_warn pr_warning

/**
 * print notice log message
 */
#define pr_notice(fmt, ...)  						\
		do {										\
			LOG_DATE;								\
			sh_log_printf(CONSOLE_NOTICE);			\
			sh_log_printf(fmt, ##__VA_ARGS__);		\
			sh_log_printf(SH_NL_STR);				\
		}while(0)

/**
 * print info log message
 */
#define pr_info(fmt, ...)  							\
		do {										\
			LOG_DATE;								\
			sh_log_printf(CONSOLE_INFO);			\
			sh_log_printf(fmt, ##__VA_ARGS__);		\
			sh_log_printf(SH_NL_STR);				\
		}while(0)

#ifndef DEBUG_MODULE
#define pr_debug(fmt, ...)
#else

/**
 * print debug log message
 */
#define pr_debug(fmt, ...)  						\
		do {										\
			LOG_DATE;								\
			sh_log_printf(CONSOLE_DEBUG);			\
			sh_log_printf(fmt, ##__VA_ARGS__);		\
			sh_log_printf(SH_NL_STR);				\
		}while(0)
#endif // DEBUG_MODULE

#endif //IF USE_STANDARD_PRINTF

typedef void(*shell_cmd_t)(int argc, char *argv[]);

#define MAX_EXTERNAL_CHAR_HANDLERS 4

typedef uint8_t (*external_char_handler_t)(char c);

typedef struct {
	char *command_name;
	shell_cmd_t function;
	uint8_t only_for_root;
}shell_command_t;

typedef struct {
	UART_HandleTypeDef *uart;
	shell_command_t *commands;
	uint16_t stack_size;
	uint8_t priority;
	void *rtc;
	uint16_t history_buffer_size;
	char *app_title;
} shell_configuration_t;

/**
 * Initialize shell subsytem
 *
 * Shell uses an own thread to service console
 *
 * \param cfg shell configuration parameters
 * \return pdTRUE if successfully or pdFAIL of soemthing went wrong
 */
BaseType_t sh_init(shell_configuration_t *cfg);

/**
 * put a character to the console buffer
 * \param ch the character
 */
BaseType_t sh_putchar(uint8_t ch);

/**
 * Interrupt function when some characters coming from console serial port
 * \param huart pointer to the console serial port
 */
void sh_receive_complete(UART_HandleTypeDef *huart);

/**
 * toggle log printing mode
 */
void sh_toggle_log();

/**
 * Set log printing mode off
 */
void sh_log_off();

/**
 * Set log printing mode on
 *
 */
void sh_log_on();

/**
 *
 */
void sh_log_get_date_and_time();

#if defined(DATE_IN_LOG_MESSAGES)
BaseType_t sh_set_rtc(uint16_t year,uint8_t month, uint8_t day, uint8_t week_day, uint8_t hour, uint8_t min, uint8_t sec);
#endif

int sh_vprintf(const char *fmt, va_list ap);

/**
 * system wise used printf
 * if you want to use floating point for formatting, you need to set STPRINTF_USE_FLOAT
 * @see STPRINTF_USE_FLOAT
 * \param fmt format
 * \param ... values for format
 * \return the number of characters to printing
 */
int sh_printf(const char *fmt, ...);

int sh_snprintf(char *str, size_t size, const char *fmt, ...);

/**
 * print a log message.
 * if log is disabled, the message cannot be printed
 * \param fmt format
 * \param ... arguments for formatting
 */
int sh_log_printf(const char *fmt, ...);

/**
 * dump memory hexadecimal values from address with length
 *
 * Be carefull, because there are memory segments, where reading generates a segfault!
 *
 * \param address the address of memory
 * \param length length of dump
 */
void sh_dump_memory(uint8_t *address, uint16_t length);

void sh_register_external_char_handler(external_char_handler_t ch);

void sh_deregister_external_char_handler(external_char_handler_t ch);

/**
 * Dump memory by the given parameters
 *
 * command format: command <starting address> [length]
 *
 * default value of length is 16
 * @see DEFAULT_DUMP_LENGTH
 *
 * \param argc the number of arguments
 * \param argv the arguments
 */
void cmd_dump_memory(int argc, char *argv[]);

#define SHELL_CONFIG(__uart__,__commands__,__stack__,__priority__,__rtc__,__history_buffer__,__app_title__)   		\
shell_configuration_t shell_config= {											\
 	.uart=(__uart__),															\
 	.commands=(__commands__),													\
 	.stack_size=(__stack__),													\
 	.priority=(__priority__),													\
 	.rtc=(__rtc__),																\
 	.history_buffer_size=(__history_buffer__),									\
	.app_title=(__app_title__)													\
}


#ifdef __cplusplus
}
#endif

/**
 * @}
 */

/**
 * @}
 */
#endif /* SHELL_H_ */
