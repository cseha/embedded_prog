/*
 * button.h
 *
 *  Created on: 31 Oct 2020
 *      Author: mark
 */

#ifndef INC_BUTTON_H_
#define INC_BUTTON_H_

#include "FreeRTOS.h"
#include "shell.h"

#define SHELL_COMMAND_STATISTICS "stat"

#define STATISTICS_COMMANDS \
{ .command_name = SHELL_COMMAND_STATISTICS, .function = cmd_statistics}

void button_task();

void cmd_statistics(int argc, char **argv);

#endif /* INC_BUTTON_H_ */
