/*
 * analog.h
 *
 *  Created on: Oct 20, 2020
 *      Author: mark
 */

#ifndef INC_ANALOG_H_
#define INC_ANALOG_H_

#include "FreeRTOS.h"
#include "shell.h"

#define TASK_COMMAND_AD "ad"
#define ANALOG_COMMANDS \
{ .command_name = TASK_COMMAND_AD, .function = cmd_analog}

#define AD_CHANNELS (1)

BaseType_t ad_init();

BaseType_t ad_deinit();

void cmd_analog(int argc, char **argv);

void ad_processing_task(void const *argument);

#endif /* INC_ANALOG_H_ */
