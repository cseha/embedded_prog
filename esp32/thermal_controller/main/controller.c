/*
 * controller.c
 *
 *  Created on: Dec 18, 2020
 *      Author: mark
 */

#include <string.h>
#include <time.h>

#include "argtable3/argtable3.h"
#include "driver/gpio.h"
#include "hal/gpio_types.h"
#include "measuring.h"
#include "controller.h"

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
//#define LOG_LOCAL_LEVEL ESP_LOG_ERROR
#include "esp_log.h"

#define CONTROLLER_STACK_SIZE (2048)
#define CONTROLLER_TASK_DELAY_MS (100)
#define TEMPERATURE_DIFFERENCE ((double)5.0)
#define T_TAG "controller task"

typedef enum {
	OFF = 0,
	ON
}state_t;

typedef struct{
	uint16_t dig_value;
	double temp_value;
}temperature_t;

typedef struct {
	temperature_t Tfloor;
	temperature_t Tboyler;
	state_t pump_state;
	time_t last_on;
}controller_t;

static gpio_config_t pump_output = {
		.pin_bit_mask = GPIO_SEL_26,
		.mode = GPIO_MODE_OUTPUT,
		.pull_up_en = GPIO_PULLUP_DISABLE,
		.pull_down_en = GPIO_PULLDOWN_DISABLE,
		.intr_type = GPIO_INTR_DISABLE
};

static TaskHandle_t controller_task_handler;
static controller_t control;
//extern SemaphoreHandle_t mutex;

static void fn_controller(){
	ESP_LOGD(T_TAG, "entering fn_controller");
	time_t current_time;
	for(;;){
//		xSemaphoreTake(mutex, 100);
		control.Tboyler.dig_value = analog_get(BOYLER_CHANNEL, 100);
		control.Tboyler.temp_value = temperature_get(BOYLER_CHANNEL, control.Tboyler.dig_value);
		//ESP_LOGD(TAG, "Boyler temperature: %.2fC", control.Tboyler.temp_value);

		control.Tfloor.dig_value = analog_get(FLOOR_CHANNEL, 100);
		control.Tfloor.temp_value = temperature_get(FLOOR_CHANNEL, control.Tfloor.dig_value);
		//ESP_LOGD(TAG, "Floor temperature: %.2fC", control.Tfloor.temp_value);
		time(&current_time);
		if(current_time-control.last_on >= SWITCH_WAIT_TIME_SEC){
			if(control.Tfloor.temp_value+TEMPERATURE_DIFFERENCE < control.Tboyler.temp_value){
				if(control.pump_state == OFF){
					if(gpio_set_level(GPIO_NUM_26, ON) != ESP_OK)
						ESP_LOGE(T_TAG, "GPIO set error");

					ESP_LOGI(T_TAG, "#####PUMP switched ON after %ld seconds at FLOOR: %.2fC, BOYLER: %.2fC#####", current_time-control.last_on, control.Tfloor.temp_value, control.Tboyler.temp_value);
					control.pump_state = ON;
					control.last_on = current_time;
				}
			}else if(control.Tfloor.temp_value >= control.Tboyler.temp_value+TEMPERATURE_DIFFERENCE){
				if(control.pump_state == ON){
					if(gpio_set_level(GPIO_NUM_26, OFF) != ESP_OK)
						ESP_LOGE(T_TAG, "GPIO set error");

					ESP_LOGI(T_TAG, "#####PUMP switched OFF after %ld seconds at FLOOR %.2fC, BOYLER: %.2fC#####", current_time-control.last_on, control.Tfloor.temp_value, control.Tboyler.temp_value);
					control.pump_state = OFF;
					control.last_on = current_time;
				}
			}
		}

//		xSemaphoreGive(mutex);
		vTaskDelay(pdMS_TO_TICKS(CONTROLLER_TASK_DELAY_MS));
	}
}

void controller_init(){
	bzero(&control, (sizeof(control)));

	control.Tboyler.dig_value = analog_get(BOYLER_CHANNEL, 100);
	control.Tboyler.temp_value = temperature_get(BOYLER_CHANNEL, control.Tboyler.dig_value);

	control.Tfloor.dig_value = analog_get(FLOOR_CHANNEL, 100);
	control.Tfloor.temp_value = temperature_get(FLOOR_CHANNEL, control.Tfloor.dig_value);

	if(gpio_config(&pump_output) != ESP_OK)
		ESP_LOGE(T_TAG, "GPIO config error");

	gpio_set_level(GPIO_NUM_26, 0);

	xTaskCreate(fn_controller, "controller", CONTROLLER_STACK_SIZE, NULL, uxTaskPriorityGet(NULL), &controller_task_handler);
}

