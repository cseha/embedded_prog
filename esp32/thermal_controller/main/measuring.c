/*
 * analog.c
 *
 *  Created on: Nov 17, 2020
 *      Author: mark
 *
 *   			 + 3V3    		 GND +
 *	 	  		 + EN    		IO23 +
 *	 			 + SVP   		IO22 +
 *				 + SVN   		TXD0 +
 *	 CH1_port6<--+ IO34  		RXD0 +
 *	 CH2_port7<--+ IO35  		IO21 +
 *	 			 + IO32  		 GND +
 * 	 CH0_port5<--+ IO33  		IO19 +
 *	    		 + IO25  		IO18 +
 *	     		 + IO26  		 IO5 +
 *	   			 + IO27  		IO17 +
 *	     		 + IO14   		IO16 +
 *	    		 + IO12  		 IO4 +
 *	 			 + GND   		 IO0 +
 *	     		 + IO13  		 IO2 +
 *        		 + SD2    		 O15 +
 *       		 + SD3   		 SD1 +
 *        		 + CMD    		 SD0 +
 *      		 + 5V    		 CLK +
 */

#include "measuring.h"
#include "controller.h"
#include <string.h>
#include "driver/adc.h"
#include "console.h"
#include "cmd_system.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"
#include "nvs_flash.h"
#include "nvs.h"

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
//#define LOG_LOCAL_LEVEL ESP_LOG_ERROR
#include "esp_log.h"
#define M_TAG "measuring task"
#define N_TAG "NVS"

#define MEASURING_STACK_SIZE (2048)
#define MEASURING_TASK_DELAY_MS (100)
#define MOVING_AVERAGE_QUEUE_SIZE (10)
#define MOVING_HYSTERESIS_DELTA ((uint8_t)(60))
#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))

typedef struct{
	int16_t min;
	uint16_t max;
	uint16_t delta;
}moving_hysteresis_t;

typedef struct{
	uint16_t queue[MOVING_AVERAGE_QUEUE_SIZE];
	uint8_t index;
}moving_average_t;

typedef struct{
	uint16_t d[2];
	double t[2];
	double tg_alpha;
	uint8_t verified[2];
}verification_t;

typedef struct{
	volatile uint8_t running;
	uint32_t conversions;
	uint16_t raw;
	uint16_t normalized;
	moving_hysteresis_t moving_hyst;
	moving_average_t moving_avg;
	verification_t verification;
	adc1_channel_t port;
}ad_channel_t;

typedef enum{
	DEVICE_UNVERIFIED = 0,
	DEVICE_VERIFIED
}device_verified_t;

typedef enum{
	MIN = 0,
	MAX
}minmax_t;

typedef enum {
	TEMP_VAL = 0,
	STAT_VAL
}print_mode_t;

typedef struct{
	device_verified_t device;
	uint8_t need_store;
	uint8_t controller_running;
}meta_control_t;

adc1_channel_t channel_port[] = {
		ADC1_CHANNEL_5, //IO33
		ADC1_CHANNEL_6 //IO34
};

static TaskHandle_t measuring_task_handler;
static ad_channel_t ad_channels[CHANNELS];
static meta_control_t meta_control;

//==========COMMAND LINE ARGS==========
static struct {
	struct arg_lit *help;
	struct arg_lit *start;
	struct arg_lit *stat;
	struct arg_lit *stop;
	struct arg_lit *val;
	struct arg_lit *show;
	struct arg_int *channel;
	struct arg_lit *set;
	struct arg_int *get;
	struct arg_int *min;
	struct arg_int *max;
	struct arg_int *delta;
	struct arg_end *end;
}ad_args;

static struct {
	struct arg_lit *help;
	struct arg_lit *save;
	struct arg_lit *reset;
	struct arg_lit *erase;
	struct arg_end *end;
}nvs_args;

static void register_ad_cmd();
static int cmd_ad(int argc, char **argv);

static void register_nvs_cmd();
static int cmd_nvs(int argc, char **argv);

static void verify_set(uint8_t channel, double temp, uint16_t dig_val, minmax_t minmax);
//==========COMMAND LINE ARGS END==========

/**
 * ======================================================================================================= *
 * =============================================FLASH CONTROL============================================= *
 * ======================================================================================================= *
 */
#define KEY_BOYLER_T0 "bt0"
#define KEY_BOYLER_T1 "bt1"
#define KEY_BOYLER_D0 "bd0"
#define KEY_BOYLER_D1 "bd1"
#define KEY_BOYLER_TG_ALPHA "btg"

#define KEY_FLOOR_T0 "ft0"
#define KEY_FLOOR_T1 "ft1"
#define KEY_FLOOR_D0 "fd0"
#define KEY_FLOOR_D1 "fd1"
#define KEY_FLOOR_TG_ALPHA "ftg"

#define KEY_BOYLER_MHYS_DELTA "bdelta"
#define KEY_FLOOR_MHYS_DELTA "fdelta"

#define DBL_MULTIPLIER (1000)

#define LOAD_KEY_NUMBER(handler, key, value)					\
	do {														\
		esp_err_t err = nvs_get_u16((handler), (key), &value);	\
		if(err != ESP_OK)										\
			goto exit;											\
	}while(0)

#define LOAD_KEY_FL(handler, key, value)						\
	do{															\
		int fi;													\
		esp_err_t err = nvs_get_i32((handler), (key), &fi);		\
		(value) = fi / (double)DBL_MULTIPLIER;					\
		if(err != ESP_OK)										\
			goto exit;											\
	}while(0)

#define VERIFY_DEVICE															\
	do{																			\
		if(	ad_channels[BOYLER_CHANNEL].verification.verified[MIN] == 1			\
			&&ad_channels[FLOOR_CHANNEL].verification.verified[MIN] == 1			\
			&&ad_channels[BOYLER_CHANNEL].verification.verified[MAX] == 1 		\
			&&ad_channels[FLOOR_CHANNEL].verification.verified[MAX] == 1){		\
			meta_control.device = DEVICE_VERIFIED;								\
			ESP_LOGI(M_TAG, "device is verified");								\
		}																		\
	}while(0)

static BaseType_t load_params(){
	ESP_LOGD(N_TAG, "load_params");
	nvs_handle_t handle;
	esp_err_t err = nvs_open(N_TAG, NVS_READONLY, &handle);
	if(err != ESP_OK){
		ESP_LOGE(N_TAG, "cannot open nvs for read, err: %d", err);
		return pdFAIL;
	}

	LOAD_KEY_NUMBER(handle, KEY_BOYLER_D0, ad_channels[BOYLER_CHANNEL].verification.d[MIN]);
	LOAD_KEY_NUMBER(handle, KEY_BOYLER_D1, ad_channels[BOYLER_CHANNEL].verification.d[MAX]);
	LOAD_KEY_FL(handle, KEY_BOYLER_T0, ad_channels[BOYLER_CHANNEL].verification.t[MIN]);
	LOAD_KEY_FL(handle, KEY_BOYLER_T1, ad_channels[BOYLER_CHANNEL].verification.t[MAX]);
	LOAD_KEY_NUMBER(handle, KEY_BOYLER_MHYS_DELTA, ad_channels[BOYLER_CHANNEL].moving_hyst.delta);

	LOAD_KEY_NUMBER(handle, KEY_FLOOR_D0, ad_channels[FLOOR_CHANNEL].verification.d[MIN]);
	LOAD_KEY_NUMBER(handle, KEY_FLOOR_D1, ad_channels[FLOOR_CHANNEL].verification.d[MAX]);
	LOAD_KEY_FL(handle, KEY_FLOOR_T0, ad_channels[FLOOR_CHANNEL].verification.t[MIN]);
	LOAD_KEY_FL(handle, KEY_FLOOR_T1, ad_channels[FLOOR_CHANNEL].verification.t[MAX]);
	LOAD_KEY_NUMBER(handle, KEY_FLOOR_MHYS_DELTA, ad_channels[FLOOR_CHANNEL].moving_hyst.delta);

	verify_set(BOYLER_CHANNEL, ad_channels[BOYLER_CHANNEL].verification.t[MIN], ad_channels[BOYLER_CHANNEL].verification.d[MIN], MIN);
	verify_set(BOYLER_CHANNEL, ad_channels[BOYLER_CHANNEL].verification.t[MAX], ad_channels[BOYLER_CHANNEL].verification.d[MAX], MAX);
	verify_set(FLOOR_CHANNEL, ad_channels[FLOOR_CHANNEL].verification.t[MIN], ad_channels[FLOOR_CHANNEL].verification.d[MIN], MIN);
	verify_set(FLOOR_CHANNEL, ad_channels[FLOOR_CHANNEL].verification.t[MAX], ad_channels[FLOOR_CHANNEL].verification.d[MAX], MAX);
	VERIFY_DEVICE;
	controller_init();
	meta_control.controller_running = 1;
	nvs_close(handle);
	return pdPASS;

exit:
	nvs_close(handle);
	return pdFAIL;

}

#define STORE_KEY_NUMBER(handle, key, func, value)						\
	do{																	\
		err = func((handle), (key), (value));							\
		if(err != ESP_OK){												\
			ESP_LOGE(N_TAG, "cannot store %s, err: %d", (key), err);	\
			goto error_exit;											\
		}																\
	}while(0)

#define STORE_KEY_STRING(handle, key, value)							\
	do{																	\
		esp_err_t err = nvs_set_str((handle), (key), (value));			\
		if(err != ESP_OK){												\
			ESP_LOGE(N_TAG, "cannot store %s, err: %d", (value), err);	\
			goto error_exit;											\
		}																\
	}while(0);

static BaseType_t store_params(){
	ESP_LOGD(N_TAG, "store_params");
	if(!meta_control.need_store){
		ESP_LOGW(N_TAG, "no changes to save");
		return pdFAIL;
	}

	nvs_handle_t handle;
	esp_err_t err = nvs_open(N_TAG, NVS_READWRITE, &handle);
	if(err != ESP_OK){
		ESP_LOGE(N_TAG, "cannot open nvs for readwrite, err: %d", err);
		return pdFAIL;
	}

	STORE_KEY_NUMBER(handle, KEY_BOYLER_D0, nvs_set_u16, ad_channels[BOYLER_CHANNEL].verification.d[MIN]);
	STORE_KEY_NUMBER(handle, KEY_BOYLER_D1, nvs_set_u16, ad_channels[BOYLER_CHANNEL].verification.d[MAX]);
	STORE_KEY_NUMBER(handle, KEY_BOYLER_T0, nvs_set_i32, ad_channels[BOYLER_CHANNEL].verification.t[MIN]*DBL_MULTIPLIER);
	STORE_KEY_NUMBER(handle, KEY_BOYLER_T1, nvs_set_i32, ad_channels[BOYLER_CHANNEL].verification.t[MAX]*DBL_MULTIPLIER);
	STORE_KEY_NUMBER(handle, KEY_BOYLER_MHYS_DELTA, nvs_set_u16, ad_channels[BOYLER_CHANNEL].moving_hyst.delta);

	STORE_KEY_NUMBER(handle, KEY_FLOOR_D0, nvs_set_u16, ad_channels[FLOOR_CHANNEL].verification.d[MIN]);
	STORE_KEY_NUMBER(handle, KEY_FLOOR_D1, nvs_set_u16, ad_channels[FLOOR_CHANNEL].verification.d[MAX]);
	STORE_KEY_NUMBER(handle, KEY_FLOOR_T0, nvs_set_i32, ad_channels[FLOOR_CHANNEL].verification.t[MIN]*DBL_MULTIPLIER);
	STORE_KEY_NUMBER(handle, KEY_FLOOR_T1, nvs_set_i32, ad_channels[FLOOR_CHANNEL].verification.t[MAX]*DBL_MULTIPLIER);
	STORE_KEY_NUMBER(handle, KEY_FLOOR_MHYS_DELTA, nvs_set_u16, ad_channels[FLOOR_CHANNEL].moving_hyst.delta);

	err = nvs_commit(handle);
	if(err != ESP_OK){
		ESP_LOGE(N_TAG, "cannot commit, err: %d", err);
		goto error_exit;
	}

	nvs_close(handle);
	ESP_LOGI(N_TAG, "data saved to flash");
	meta_control.need_store = 0;
	return pdPASS;

error_exit:
	nvs_close(handle);
	return pdFAIL;
}

BaseType_t flash_init(){
	meta_control.need_store = 0;
	if(load_params() != pdPASS){
		ESP_LOGW(N_TAG, "falsh is empty, device is not verified");
	}
	return pdPASS;
}

/**
 * ======================================================================================================= *
 * =============================================ANALOG CONTROL============================================ *
 * ======================================================================================================= *
 */
uint16_t analog_get(uint8_t channel, BaseType_t wait_tick){
	uint16_t result = ad_channels[channel].normalized;
	return result;
}

static inline BaseType_t checkChannel(uint8_t channel){
	if(channel >= CHANNELS){
		ESP_LOGW(M_TAG, "channel range error");
		return pdFALSE;
	}
	return pdTRUE;
}

const char* getADC1ChannelPortName(int portname){
	switch(portname){
	case 0:
		return "ADC1_CHANNEL_0";
	case 1:
		return "ADC1_CHANNEL_1";
	case 2:
		return "ADC1_CHANNEL_2";
	case 3:
		return "ADC1_CHANNEL_3";
	case 4:
		return "ADC1_CHANNEL_4";
	case 5:
		return "ADC1_CHANNEL_5";
	case 6:
		return "ADC1_CHANNEL_6";
	case 7:
		return "ADC1_CHANNEL_7";
	case 8:
		return "ADC1_CHANNEL_MAX";
	}
	return "PORT error";
}

static uint16_t get_moving_hysteresis(uint16_t input, uint8_t channel){
	if(input > ad_channels[channel].moving_hyst.max){
		ad_channels[channel].moving_hyst.max = MIN(MAX_DIGITAL, ad_channels[channel].moving_hyst.max+ad_channels[channel].moving_hyst.delta);
		ad_channels[channel].moving_hyst.min = ad_channels[channel].moving_hyst.max-ad_channels[channel].moving_hyst.delta*2;
		return ad_channels[channel].moving_hyst.max;
	}

	if(input < ad_channels[channel].moving_hyst.min){
		if(ad_channels[channel].moving_hyst.min>MIN_DIGITAL+ad_channels[channel].moving_hyst.delta){
			ad_channels[channel].moving_hyst.min -= ad_channels[channel].moving_hyst.delta;
			ad_channels[channel].moving_hyst.max = ad_channels[channel].moving_hyst.min+ad_channels[channel].moving_hyst.delta*2;
		}
		return ad_channels[channel].moving_hyst.min;
	}
	return input;
}

static uint16_t get_moving_average(uint16_t input, uint8_t channel){
	ad_channels[channel].moving_avg.queue[ad_channels[channel].moving_avg.index] = input;
	ad_channels[channel].moving_avg.index = (ad_channels[channel].moving_avg.index+1)%MOVING_AVERAGE_QUEUE_SIZE;

	uint32_t sum = 0;
	for(uint8_t i = 0; i < MOVING_AVERAGE_QUEUE_SIZE; ++i)
		sum += ad_channels[channel].moving_avg.queue[i];

	return sum / MOVING_AVERAGE_QUEUE_SIZE;
}

static void fn_measuring(void *args){
	ESP_LOGD(M_TAG, "entering fn_analog");
	for(;;){
		for(uint8_t channel = 0; channel < CHANNELS; channel++){
			if(ad_channels[channel].running){
				ad_channels[channel].raw = adc1_get_raw(channel_port[channel]);
				ad_channels[channel].normalized = get_moving_average(get_moving_hysteresis(ad_channels[channel].raw, channel), channel);
				++ad_channels[channel].conversions;
				//ESP_LOGD(TAG, "CHANNEL %d: raw value:%d, normalized value:%d, port: %s\n",
				//		channel, ad_channels[channel].raw, ad_channels[channel].normalized, getADC1ChannelPortName(&ad_channels[channel].port));
			}
		}
		vTaskDelay(pdMS_TO_TICKS(MEASURING_TASK_DELAY_MS));
	}
}

void analog_init(){
	register_ad_cmd();
	register_nvs_cmd();
	ad_args.channel->count = CHANNELS;
	adc1_config_width(ADC_WIDTH_BIT_12);
	bzero(&meta_control, (sizeof(meta_control)));
	for(uint8_t channel = 0; channel < CHANNELS; channel++){
		bzero(&ad_channels[channel], (sizeof(ad_channels[channel])));
		adc1_config_channel_atten(channel_port[channel], ADC_ATTEN_6db);

		uint16_t raw_1 = adc1_get_raw(channel_port[channel]);
		ad_channels[channel].moving_hyst.min = raw_1 - MOVING_HYSTERESIS_DELTA;
		ad_channels[channel].moving_hyst.max = raw_1 + MOVING_HYSTERESIS_DELTA;
		ad_channels[channel].moving_hyst.delta = MOVING_HYSTERESIS_DELTA;
		ad_channels[channel].running = 1;
		ad_channels[channel].port = channel_port[channel];
	}

	xTaskCreate(fn_measuring, "measuring", MEASURING_STACK_SIZE, NULL, uxTaskPriorityGet(NULL), &measuring_task_handler);
}

/**=================================================================================================================
 * ===============================================ANALOG COMMAND LINE===============================================
 * =================================================================================================================
 */
#define A_C_TAG "AD command"
#define CMD_AD "ad"
#define HINT_AD "ad <channel x> <start|stop|show|val|stat|> <set min|max x> <get x>"

#define HELP_AD "AD commands:\n"												\
				"--start, --stop: start or stop conversion\n"					\
				"--show -w: show channel values\n"								\
				"--stat -t: print adc statistics\n"								\
				"--channel -c x: select channel\n"								\
				"\t--set, -s: set validation values\n"							\
				"\t--min -n, --max -x: select points for --set\n"				\
				"\t--get -g: print temperature value of given digital value\n"	\
				"--val -v: validate device and start pump control task \n"

#define AD_ARG_HELP "help"
#define AD_HINT_HELP "print A/D help"

#define AD_ARG_START "start"
#define AD_HINT_START "start ad conversion on selected channel"

#define AD_ARG_STOP "stop"
#define AD_HINT_STOP "stop ad conversion on selected channel"

#define AD_ARG_SHOW "show"
#define AD_HINT_SHOW "show ad values on selected channel"

#define AD_ARG_CHANNEL "channel"
#define AD_HINT_CHANNEL "select a channel"

#define AD_ARG_SET "set"
#define AD_HINT_SET "set min or max"

#define AD_ARG_GET "get"
#define AD_HINT_GET "get temp val based on dig input"

#define AD_ARG_MIN "min"
#define AD_HINT_MIN "set min measured temp"

#define AD_ARG_MAX "max"
#define AD_HINT_MAX "set max measured temp"

#define AD_ARG_STAT "stat"
#define AD_HINT_STAT "print stats of adc channel"

#define AD_ARG_VAL "val"
#define AD_HINT_VAL "validates device and start pump control"

#define AD_ARG_DELTA "delta"
#define AD_HINT_DELTA "set new moving hysteresis delta"

#define DATA_TYPE "<n>"


//================COMMAND FUNCTIONS================
static void register_ad_cmd() {
	ad_args.help=arg_lit0("hH", AD_ARG_HELP, AD_HINT_HELP);
	ad_args.start=arg_litn(NULL, AD_ARG_START, 0, 1, AD_HINT_START);
	ad_args.stat=arg_litn("tT", AD_ARG_STAT, 0, 1, AD_HINT_STAT);
	ad_args.stop=arg_litn(NULL, AD_ARG_STOP, 0, 1, AD_HINT_STOP);
	ad_args.val=arg_litn("vV", AD_ARG_VAL, 0, 1, AD_HINT_VAL);
	ad_args.show=arg_litn("wW", AD_ARG_SHOW, 0, 1, AD_HINT_SHOW);
	ad_args.set = arg_litn("sS", AD_ARG_SET, 0, 1, AD_HINT_SET);
	ad_args.get = arg_intn("gG", AD_ARG_GET, DATA_TYPE, 0, 1, AD_HINT_GET);
	ad_args.min = arg_intn("nN", AD_ARG_MIN, DATA_TYPE, 0, 1, AD_HINT_MIN);
	ad_args.max = arg_intn("xX", AD_ARG_MAX, DATA_TYPE, 0, 1, AD_HINT_MAX);
	ad_args.delta = arg_intn("dD", AD_ARG_DELTA, DATA_TYPE, 0, 1, AD_HINT_DELTA);
	ad_args.channel=arg_intn("cC", AD_ARG_CHANNEL, "", 0, 1, AD_HINT_CHANNEL);
	ad_args.end=arg_end(0);

	const esp_console_cmd_t cmd = {
			.command=CMD_AD,
			.help=HELP_AD,
			.hint=HINT_AD,
			.func=&cmd_ad
	};

	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

static void printAllChannels(print_mode_t print_mode){
	for(int channel = 0; channel < CHANNELS; channel++){
		if(print_mode == TEMP_VAL){
			printf("CHANNEL %d - %s|\t dig val -  raw: %d, norm %d| temp val: %.2f| port: %s\n",
					channel, channel ? "FLOOR" : "BOYLER",
					ad_channels[channel].raw, ad_channels[channel].normalized,
					temperature_get(channel, ad_channels[channel].normalized),
					getADC1ChannelPortName(ad_channels[channel].port));
		}

		if(print_mode == STAT_VAL){
			printf("CHANNEL %d - port: %s| running: %s| conversions: %d| hys delta: %d| tg alpha: %.4f\n"
				"minval verification: %s|\t d[MIN]: %d|\t\t t[MIN]: %.2fC\n"
				"maxval verification: %s|\t d[MAX]: %d|\t\t t[MAX]: %.2fC\n",
				channel, getADC1ChannelPortName(ad_channels[channel].port), channel ? "FLOOR" : "BOYLER", ad_channels[channel].conversions,
				ad_channels[channel].moving_hyst.delta, ad_channels[channel].verification.tg_alpha,
				ad_channels[channel].verification.verified[MIN] ? "true" : "false", ad_channels[channel].verification.d[MIN], ad_channels[channel].verification.t[MIN],
				ad_channels[channel].verification.verified[MAX] ? "true" : "false", ad_channels[channel].verification.d[MAX], ad_channels[channel].verification.t[MAX]);
		}
	}
}

double  temperature_get(uint8_t channel, uint16_t dig_input){
	if(checkChannel(channel)==pdFALSE)
		return -9999.99f;

	if(ad_channels[channel].verification.verified[MIN] == 0 || ad_channels[channel].verification.verified[MAX] == 0){
		ESP_LOGW(M_TAG, "CHANNEL %d is not verified", channel);
		return -9999.99f;
	}

	verification_t *v = &ad_channels[channel].verification;

	return v->t[MIN] + (dig_input-v->d[MIN])*v->tg_alpha;
}

static void calc_tg_alpha(uint8_t channel){
	if(checkChannel(channel)==pdFALSE)
		return;

	verification_t *v = &ad_channels[channel].verification;
	if (v->d[MAX] > v->d[MIN] &&
		v->t[MAX] > v->t[MIN] &&
		v->d[MAX] <= MAX_DIGITAL &&
		v->t[MIN] >= MIN_TEMPERATURE && v->t[MAX] <= MAX_TEMPERATURE)
			ad_channels[channel].verification.tg_alpha=(v->t[MAX]-v->t[MIN])/(double)(v->d[MAX]-v->d[MIN]);

}

static void verify_set(uint8_t channel, double temp, uint16_t dig_val, minmax_t minmax){
	if(checkChannel(channel)==pdFAIL)
		return;

	ad_channels[channel].verification.d[minmax] = dig_val;
	ad_channels[channel].verification.t[minmax] = temp;
	ad_channels[channel].verification.verified[minmax] = 1;
	ESP_LOGI(M_TAG, "Verifying CHANNEL %d %s value: temperature: %.2fC digital: %d.",
			channel, minmax ? "maximum" : "minimum", ad_channels[channel].verification.t[minmax], ad_channels[channel].verification.d[minmax]);

	calc_tg_alpha(channel);
}



static int cmd_ad(int argc, char **argv){
	int nerrors = arg_parse(argc, argv, (void **) &ad_args);
	ESP_LOGI(A_C_TAG, "AD command, argc: %d.", argc);

	if (nerrors || argc<=1 || ad_args.help->count>0){
		printf(HELP_AD);
		return 1;
	}

	if (ad_args.show->count > 0 && argc == 2){
		printAllChannels(TEMP_VAL);
		return 0;
	}

	if (ad_args.stat->count > 0 && argc == 2){
		printAllChannels(STAT_VAL);
		return 0;
	}

	if (ad_args.val->count > 0 && argc == 2){
		if(meta_control.controller_running ==0){
			VERIFY_DEVICE;
			if(meta_control.device == DEVICE_VERIFIED){
				controller_init();
				meta_control.controller_running = 1;
				return 0;
			}
			ESP_LOGW(A_C_TAG, "Cannot validate device, some channels are not verified.");
			return 1;
		}
		ESP_LOGW(A_C_TAG, "Device is already validated, control task is running.");
		return 1;
	}

	if (ad_args.channel->count == 0 || checkChannel(ad_args.channel->ival[0]) != pdTRUE){
		ESP_LOGW(A_C_TAG, "You have to set channel with -c or --channel from %d up to %d.", 0, CHANNELS-1);
		return 1;
	}

	uint8_t channel = ad_args.channel->ival[0];


	if (ad_args.start->count > 0 && argc == 4){
		ad_channels[channel].running = 1;

		ESP_LOGI(A_C_TAG, "CHANNEL %d conversion started - port: %s.", channel, getADC1ChannelPortName(ad_channels[channel].port));
		return 0;
	}

	if (ad_args.stop->count>0 && argc == 4){
		ad_channels[channel].running = 0;

		ESP_LOGI(A_C_TAG, "CHANNEL %d conversion stopped - port: %s.", channel, getADC1ChannelPortName(ad_channels[channel].port));
		return 0;
	}

	if(ad_args.set->count>0 && argc == 6){
		if(ad_args.min->count>0){
			int temp = ad_args.min->ival[0];
			if(MIN_TEMPERATURE > temp || temp > MAX_TEMPERATURE){
				ESP_LOGW(A_C_TAG, "Invalid temperature value, correct interval [%d,%d].", MIN_TEMPERATURE, MAX_TEMPERATURE);
				return 1;
			}
			meta_control.need_store = 1;
			verify_set(channel, (double)temp, analog_get(channel, 100), MIN);
			return 0;
		}
		if(ad_args.max->count>0){
			int temp = ad_args.max->ival[0];
			if(MIN_TEMPERATURE > temp || temp > MAX_TEMPERATURE){
				ESP_LOGW(A_C_TAG, "Invalid temperature value, correct interval [%d,%d].", MIN_TEMPERATURE, MAX_TEMPERATURE);
				return 1;
			}
			meta_control.need_store = 1;
			verify_set(channel, (double)temp, analog_get(channel, 100), MAX);
			return 0;
		}
		if(ad_args.delta->count > 0){
			ad_channels[channel].moving_hyst.delta = ad_args.delta->ival[0];
			ESP_LOGI(A_C_TAG, "CHANNEL %d - moving hysteresis delta set to %d\n", channel, ad_channels[channel].moving_hyst.delta);
			meta_control.need_store = 1;
			return 0;
		}
		ESP_LOGW(A_C_TAG, "Need a numeric value to assign.");
		return 1;
	}

	if(ad_args.get->count>0 && argc == 5){
		int dig = ad_args.get->ival[0];

		if(MIN_DIGITAL > dig || dig > MAX_DIGITAL){
			ESP_LOGW(A_C_TAG, "Invalid digital value, correct interval [0,4096].");
			return 1;
		}

		ESP_LOGI(A_C_TAG, "CHANNEL %d: temperature %.2fC is assigned to digital value %d.\n",
				channel, temperature_get(channel, dig), dig);
		return 0;
	}

	printf(HELP_AD);
	return 1;
}
//==========AD COMMANDS END==========

/**=================================================================================================================
 * =================================================NVS COMMAND LINE================================================
 * =================================================================================================================
 */
#define N_C_TAG "NVA command"
#define CMD_NVS "nvs"
#define HINT_NVS "nvs <save|reset> <erase all|key>"

#define HELP_NVS "NVS commands:\n"																	\
				"--save -s: save verification and moving hysteresis current data to flash\n"		\
				"--erase -e: erase data from flash\n"												\
				"(--reset) -r: restore verification and moving hysteresis data form flash (should use -r)\n"

#define N_ARG_HELP "help"
#define N_HINT_HELP "print A/D help"

#define N_ARG_SAVE "save"
#define N_HINT_SAVE "save data to flash"

#define N_ARG_RESET "save"
#define N_HINT_RESET "reset data from flash"

#define N_ARG_ERASE "erase"
#define N_HINT_ERASE "erase data from flash"

static void register_nvs_cmd(){
	nvs_args.help = arg_lit0("hH", N_ARG_HELP, N_HINT_HELP);
	nvs_args.save = arg_lit0("sS", N_ARG_SAVE, N_HINT_SAVE);
	nvs_args.reset = arg_lit0("rR", N_ARG_RESET, N_HINT_RESET);
	nvs_args.erase = arg_lit0("eE", N_ARG_ERASE, N_HINT_ERASE);
	nvs_args.end = arg_end(0);

	const esp_console_cmd_t cmd = {
			.command = CMD_NVS,
			.help = HELP_NVS,
			.hint = HINT_NVS,
			.func = cmd_nvs
	};

	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

static void erase_all(){
	ESP_LOGD(N_C_TAG, "erase_all");
	nvs_handle_t handle;
	esp_err_t err = nvs_open(N_TAG, NVS_READWRITE, &handle);
	if(err != ESP_OK){
		ESP_LOGW(N_C_TAG, "cannot open flash for readwrite, err: %x", err);
		return;
	}

	err = nvs_erase_all(handle);
	if(err != ESP_OK){
		ESP_LOGW(N_C_TAG, "cannot erase flash, err %x", err);
		goto exit;
	}

	err = nvs_commit(handle);
	if(err != ESP_OK){
		ESP_LOGW(N_C_TAG, "cannot commit erase, err %d", err);
		goto exit;
	}

	ESP_LOGI(N_C_TAG, "All flash has been erased");

exit:
	nvs_close(handle);
}

static int cmd_nvs(int argc, char **argv){
	ESP_LOGD(N_C_TAG, "NVS command - argc: %d.", argc);

	int err = arg_parse(argc, argv, (void**) &nvs_args);
	if(err || argc == 1){
		printf(HELP_NVS);
		return 1;
	}

	if(nvs_args.help->count > 0 && argc == 2){
		printf(HELP_NVS);
		return 0;
	}

	if(nvs_args.save->count > 0 && argc == 2){
		if(meta_control.device == DEVICE_VERIFIED){
		store_params();
		return 0;
		}
		ESP_LOGW(N_C_TAG, "verfiy every channel and validate device before saving");
		return 1;
	}

	if(nvs_args.reset->count > 0 && argc == 2){
		ESP_LOGI(N_C_TAG, "data reset from flash: %s\n", load_params() == pdPASS ? "successful" : "empty flash");
		return 0;
	}

	if(nvs_args.erase->count > 0 && argc == 2){
		erase_all();
		return 0;
	}

	printf(HELP_NVS);
	return 1;
}
