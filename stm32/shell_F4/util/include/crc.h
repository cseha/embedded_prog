/*
 * crc.h
 *
 *  Created on: Apr 25, 2016
 *      Author: zamek
 */

#ifndef INCLUDE_CRC_H_
#define INCLUDE_CRC_H_

#include <stdint.h>
#include <unistd.h>

typedef uint16_t crc16_t;

crc16_t crc16_calc_buffer(char *data, size_t len);

/** on the fly crc calculation
    add a byte to the crc
    \param crc aggregated value of crc
    \param byte to aggregate to crc
    \return new crc value
*/
void crc16_add_byte(crc16_t *crc, unsigned char b);

/** on the fly crc calculation for Modbus
    add a byte to the crc
    \param crc aggregated value of crc
    \param byte to aggregate to crc
    \return new crc value
*/
void crc_modbus_add_byte(crc16_t *crc, uint8_t b);

/**
    Crc begin for different methods
    \param method type of crc method
    \param address of crc value
*/
void crc_modbus_begin(crc16_t *crc);

void crc16_begin(crc16_t *crc);


#endif /* INCLUDE_CRC_H_ */
