/*
 * counter.h
 *
 *  Created on: Oct 9, 2020
 *      Author: mark
 */

#ifndef INC_COUNTER_H_
#define INC_COUNTER_H_

#include "FreeRTOS.h"
#include "shell.h"

#define COUNTER_CMD "counter"

#define COUNTER_COMMANDS \
{	.command_name = COUNTER_CMD, .function = cmd_counter }

void cmd_counter(int argc, char **argv);

BaseType_t counter_init();

BaseType_t counter_deinit();

void counter_task(const void *args);

#endif /* INC_COUNTER_H_ */
