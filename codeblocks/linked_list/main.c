#include <stdio.h>
#include <stdlib.h>

#include "linked.h"

bounces_t first = {1, 30, 0};
bounces_t second = {2, 45, 1};
bounces_t third = {3, 67, 0};
bounces_t fourth = {4, 12, 1};
bounces_t fifth = {5, 14, 0};
bounces_t sixth = {6, 90, 1};

int main()
{
    queue_head_t List = {NULL, 0};
    addBounceToList(&List, first);
    printf("queue size is %d\n", List.size);
    addBounceToList(&List, second);
    printf("queue size is %d\n", List.size);
    addBounceToList(&List, third);
    printf("queue size is %d\n", List.size);
    addBounceToList(&List, fourth);
    printf("queue size is %d\n", List.size);
    addBounceToList(&List, fifth);
    printf("queue size is %d\n", List.size);
    addBounceToList(&List, sixth);
    printf("queue size is %d\n", List.size);

    printBounces(&List);

    printf("mean: %.2f\n", getBounceMean(&List));

    printf("mean: %d\n", getLastBounce(&List));

    eraseBouncesList(&List);

    printBounces(&List);




    return 0;
}





