/**
 * \file console.c
 *
 * Created Oct 30, 2016
 *
 * \author zamek
 * \author Giovanni Di Sirio
 */

#include <string.h>
#include <stdint.h>

#include <FreeRTOS.h>
#include <event_groups.h>
#include <timers.h>
#include <shell.h>
#include <task.h>
#include <semphr.h>
#include <task.h>
#include <queue.h>
#include <charqueue.h>
#include <cmsis_os.h>

/**
 * printf max filler characters
 */
#define MAX_FILLER 11

/**
 * max precision of floating  point conversion
 */
#define FLOAT_PRECISION 9

typedef uint8_t bool;

#define TRUE 1
#define FALSE 0

/**
 * log printing query/set/clear command
 * usage log [on|off]
 */
static void cmd_log(int argc, char *argv[]);

/**
 * change to superuser mode
 */
static void cmd_su(int argc, char *argv[]);

/**
 * logout from superuser mode
 */
static void cmd_logout(int argc, char *argv[]);

static void cmd_system_reset();

/**
 * print a current prompt depends on superuser mode (# or $)
 */
static inline void prompt();

/**
 * toggle log printing mode
 */
void sh_toggle_log();

static const char *ON_STR = "on";
static const char *OFF_STR = "off";
static const char *LOG_MSG = "Log is ";

/**
 * Default length of dump
 */
#define DEFAULT_DUMP_LENGTH 0x10

/**
 * number of bytes per line
 */
#define DUMP_BYTES_PER_LINE 0x10

/**
 * history shift direction
 */
typedef enum { shd_forward, shd_backward } shell_hist_dir_t;

void cmd_repeat(int argc, char *argv[]);

#define CMD_REPEAT "!"

/**
 * built in commands
 */
static shell_command_t SHELL_COMMANDS[] = {
		{ .command_name="log",    .function=cmd_log 							},
		{ .command_name="su",     .function=cmd_su 								},
		{ .command_name="logout", .function=cmd_logout,		.only_for_root=1 	},
		{ .command_name="dump",   .function=cmd_dump_memory 					},
		{ .command_name=CMD_REPEAT, .function=cmd_repeat						},
		{ .command_name="restart", .function=cmd_system_reset, .only_for_root=1 },
		{ .command_name=NULL, .function=NULL 									}
};

#define EB_TRANSMIT_QUEUE_FULL (1 << 0)
#define EB_TRANSMIT_WAIT pdMS_TO_TICKS(5)

static	shell_configuration_t *configuration;  	/** User configuration */
static	QueueHandle_t receive_queue;			/** receiving queue */
static	uint8_t receive_buffer;					/** receive buffer */
static	uint8_t console_enabled;				/** all log to console if console enabled */
static	char_queue_t transmit_queue;			/** transmit queue */
static	uint8_t super_user_mode;				/** root mode */
static	uint8_t password_mode;					/** password input mode, hide all input characters */
static	EventGroupHandle_t tx_full_event;		/** transmit buffer full event */
#if defined(SHELL_USE_COMPLETION)
static char **completion;						/** completion buffer */
#endif
#if defined(SHELL_USE_HISTORY)
static char *history_buffer;
#endif
#if defined(DATE_IN_LOG_MESSAGES)
static	RTC_HandleTypeDef *rtc;					/** real time clock */
#endif

/** repeat variables */
static uint8_t repeat_active=0;
static uint8_t repeat_counter=0;
static const uint8_t repeat_counter_reload=100;

/** External char handlers */
external_char_handler_t external_char_handlers[MAX_EXTERNAL_CHAR_HANDLERS];

typedef struct {
	char *buffer;
	const int size;
	int begin;
	int end;
	int cur;
} shell_history_t;

static const char SHELL_USER_PROMPT_STR[] =   "$> ";
static const char SHELL_ROOT_PROMPT_STR[] =   "#> ";
static const char SHELL_REPEAT_PROMPT_STR[] = "!> ";

static const int SHELL_PROMPT_LEN = sizeof(SHELL_USER_PROMPT_STR) -1;

/**
 * reset cursor
 */
static inline void  sh_reset_cursor(){
	sh_printf("\033[%dD\033[%dC",
				 SHELL_MAX_LINE_LENGTH + SHELL_PROMPT_LEN + 2,
				 SHELL_PROMPT_LEN);
}

static inline void sh_cursor_left() {
	sh_printf("\033[%dD",1);
}
/**
 * clear line
 */
static inline void sh_clr_line(){
	sh_printf("\033[K");
}

/**
 * shell usage message
 */
static inline void sh_usage(const char *message) {
	sh_printf("Usage:%s"SH_NL_STR, message);
}

/**
 * send a character to the transmit queue
 *
 * wait if the queue is full
 */
static BaseType_t tr_queue(uint8_t c) {
	while (cq_is_full(&transmit_queue))
		xEventGroupWaitBits(tx_full_event, EB_TRANSMIT_QUEUE_FULL, pdFALSE, pdFALSE, EB_TRANSMIT_WAIT);

	BaseType_t result = cq_put(&transmit_queue, c);

	if (cq_is_full(&transmit_queue))
		xEventGroupClearBits(tx_full_event, EB_TRANSMIT_QUEUE_FULL);

	return result;
}

/**
 * put a character to the transmit queue
 */
BaseType_t sh_putchar(uint8_t ch) {
	return tr_queue(ch);
}

/**
 * receive completed interrupt function
 */
void sh_receive_complete(UART_HandleTypeDef *huart) {
	BaseType_t hptw = pdFALSE;

	if (!(huart && huart == configuration->uart))
		return;

	xQueueSendFromISR(receive_queue, &receive_buffer, &hptw);
	HAL_UART_Receive_IT(configuration->uart, &receive_buffer, 1U);
}

static char *parse_arguments(char *str, char **saveptr) {
  char *p;

  if (str != NULL)
    *saveptr = str;

  p = *saveptr;
  if (!p) {
    return NULL;
  }

  /* Skipping white space.*/
  p += strspn(p, " \t");

  if (*p == '"') {
    /* If an argument starts with a double quote then its delimiter is another quote.*/
    p++;
    *saveptr = strpbrk(p, "\"");
  }
  else {
    /* The delimiter is white space.*/
    *saveptr = strpbrk(p, " \t");
  }

  /* Replacing the delimiter with a zero.*/
  if (*saveptr != NULL) {
    *(*saveptr)++ = '\0';
  }

  return *p != '\0' ? p : NULL;
}

static void list_commands(const shell_command_t *scmd) {
	while(scmd->command_name) {
		if ((super_user_mode && scmd->only_for_root) || (!scmd->only_for_root))
			sh_printf("%s ", scmd->command_name);

		scmd++;
	}
}

#if defined(SHELL_USE_HISTORY)
static void del_history_buff_entry(shell_history_t *sh) {
	int pos = sh->begin + *(sh->buffer + sh->begin) + 1;
	if (pos >= sh->size)
		pos -= sh->size;

	sh->begin = pos;
}

static uint8_t is_histbuff_space(shell_history_t *sh, int length) {
	if (sh->end >= sh->begin) {
		if (length < (sh->size - (sh->end - sh->begin + 1)))
			return 1;
	}
	else {
		if (length < (sh->begin - sh->end - 1))
			return 1;
	}

	return 0;
}

static void save_history(shell_history_t *sh, char *line, int length) {
	if (!sh || strncmp(CMD_REPEAT, line, length)==0)
		return;

	if(length > sh->size - 2)
		return;

	while((*(line + length - 1) == ' ') && (length > 0))
		length--;

	if (length <= 0)
		return;

	while(!is_histbuff_space(sh, length))
		del_history_buff_entry(sh);

	if(length < sh->size - sh->end - 1)
		memcpy(sh->buffer + sh->end + 1, line, length);
	else {
	    /*
	     * Since there isn't enough room left at the end of the buffer,
	     * split the line to save up to the end of the buffer and then
	     * wrap back to the beginning of the buffer.
	     */
		int part_len = sh->size - sh->end - 1;
		memcpy(sh->buffer + sh->end + 1, line, part_len);
		memcpy(sh->buffer, line + part_len, length - part_len);
	}

	/* Save the length of the current line and move the buffer end pointer */
	*(sh->buffer + sh->end) = (char)length;
	sh->end += length + 1;
	if(sh->end >= sh->size)
		sh->end -= sh->size;

	*(sh->buffer + sh->end) = 0;
	sh->cur = 0;
}

static int get_history(shell_history_t *sh,char *line, shell_hist_dir_t dir) {
	int count = 0;
	if (!sh)
		return -1;

	/* Count the number of lines saved in the buffer */
	int idx = sh->begin;
	while(idx != sh->end) {
		idx += *(sh->buffer + idx) + 1;
		if (idx >= sh->size)
			idx -= sh->size;
		count++;
	}

	if(dir==shd_forward) {
		if(sh->cur > 0)
			sh->cur -= 2;
		else
			return 0;
	}

	if(count >= sh->cur) {
		idx = sh->begin;
		int i = 0;
		while(idx != sh->end && sh->cur != (count - i - 1)) {
			idx += *(sh->buffer + idx) + 1;
			if (idx >= sh->size)
				idx -= sh->size;
			i++;
		}

		int length = *(sh->buffer + idx);

		if(length > 0) {
			sh->cur++;

			memset(line, 0, SHELL_MAX_LINE_LENGTH);
			if((idx + length) < sh->size)
				memcpy(line, (sh->buffer + idx + 1), length);
			else {
		        /*
		         * Since the saved line was split at the end of the buffer,
		         * get the line in two parts.
		         */
				int part_len = sh->size - idx - 1;
				memcpy(line, sh->buffer + idx + 1, part_len);
				memcpy(line + part_len, sh->buffer, length - part_len);
			}
			return length;
		}
		else if (dir==shd_backward) {
			sh->cur++;
			return 0;
		}
	}
	return -1;
}
#endif //if defined(SHELL_USE_HISTORY)


#if defined(SHELL_USE_COMPLETION)

static void get_completions(char *line) {
	shell_command_t *lcp = SHELL_COMMANDS;
	const shell_command_t *scp = configuration->commands;
	char **scmp = completion;
	char help_cmp[] = "help";

	if (strstr(help_cmp, line) == help_cmp)
		*scmp++ = help_cmp;

	while(lcp->command_name) {
		if(strstr(lcp->command_name, line) == lcp->command_name)
			*scmp++ = (char *)lcp->command_name;

		lcp++;
	}

	if (scp) {
		while(scp->command_name) {
			if(strstr(scp->command_name, line) == scp->command_name)
				*scmp++ = (char *) scp->command_name;

			scp++;
		}
	}

	*scmp = NULL;
}

static int process_completions(char *line, int length, uint16_t size) {
	char **scmp = completion;
	char **cmp = scmp+1;
	char *c = line + length;
	int clen = 0;

	if (*scmp) {
		if (!*cmp) {
			clen = strlen(*scmp);
			int i = length;
			while((c < line + clen) && (c < line + size - 1))
				*c++ = *(*scmp + i++);

			if (c < line + size - 1) {
				*c = ' ';
				clen++;
			}
		}
		else { //if(!*cmp)
			while(*(*scmp + clen)) {
				while((*(*scmp + clen) == *(*cmp + clen)) &&
					  (*(*cmp + clen)) && (*cmp))
					cmp++;

				if (!*cmp) {
					if ((c < line + size - 1) && (clen >= length))
						*c++ = *(*scmp + clen);

					cmp = scmp + 1;
					clen++;
				}
				else // if (!*cmp)
					break;
			}
		}

		*(line + clen) = 0;
	}
	return clen;
}

static void write_completions(char *line, int pos) {
	char **scmp = completion;

	if (*(scmp + 1)) {
		sh_printf(SH_NL_STR);
		while(*scmp)
			sh_printf(" %s", *scmp++);

		sh_printf(SH_NL_STR);

		prompt();
		sh_printf("%s", line);
	}
	else
		sh_printf("%s", line+pos);
}

#endif  // if defined(SHELL_USE_COMPLETION)

static BaseType_t serial_get_char(char *ch) {
        return xQueueReceive(receive_queue, ch, 0);
}

static uint8_t handle_repeat() {
	if (repeat_active && --repeat_counter <= 0) {
		repeat_counter = repeat_counter_reload;
		return 1;
	}
	return 0;
}

void sh_register_external_char_handler(external_char_handler_t ch) {
	for(int i = 0; i<MAX_EXTERNAL_CHAR_HANDLERS; ++i)
		if (!external_char_handlers[i])
			external_char_handlers[i] = ch;
}

void sh_deregister_external_char_handler(external_char_handler_t ch) {
	for(int i = 0; i<MAX_EXTERNAL_CHAR_HANDLERS; ++i)
		if (external_char_handlers[i]==ch)
			external_char_handlers[i] = NULL;
}

uint8_t handle_external_char_handler(char c) {
	for(int i = 0; i<MAX_EXTERNAL_CHAR_HANDLERS; ++i)
		if (external_char_handlers[i])
			return external_char_handlers[i](c);

	return 0;
}

/**
 * @brief   Reads a whole line from the input channel.
 * @note    Input chars are echoed on the same stream object with the
 *          following exceptions:
 *          - DEL and BS are echoed as BS-SPACE-BS.
 *          - CR is echoed as CR-LF.
 *          - 0x4 is echoed as "^D".
 *          - Other values below 0x20 are not echoed.
 *          .
 *
 * @param[in] line      pointer to the line buffer
 * @param[in] size      buffer maximum length
 * @param[in] shp       pointer to a @p ShellHistory object or NULL
 * @return              The operation status.
 * @retval true         the channel was reset or CTRL-D pressed.
 * @retval false        operation successful.
 *
 */
static uint8_t sh_get_line(char *line, uint8_t size, shell_history_t *shp) {
	char *p = line;
#if defined(SHELL_USE_ESC_SEQ)
	uint8_t escape = 0;
	uint8_t bracket = 0;
#endif

#if !defined(SHELL_USE_HISTORY)
	(void)*shp;
#endif
	char c;
	while(1) {
		if (serial_get_char(&c) != pdPASS) {

			if (handle_external_char_handler(c))		// some other character handler
				continue;

			if (handle_repeat()) {
				int len = get_history(shp, line, shd_backward);
				if (len > 0) {
					sh_reset_cursor();
					sh_clr_line();
					sh_printf("%s\r\n",line);
					p = line + len;
					save_history(shp, line, p - line);
					return 0;
				}
				else
					continue;
			}
			else {
				osDelay(10);
				continue;
			}
		}

		if (c==3) {  //^C
			repeat_active = 0;
			sh_printf(SH_NL_STR);
			prompt();
			continue;
		}

		if (c == 12) { // ^L
			sh_toggle_log();
			prompt();
			continue;
		}
#if defined(SHELL_USE_ESC_SEQ)
		if (c == 27) {
			escape = 1;
			continue;
		}
		if(escape) {
			escape = 0;
			if (c == '[') {
				escape = 1;
				bracket = 1;
				continue;
			}
			if(bracket) {
				bracket = 0;
#if defined(SHELL_USE_HISTORY)
				if(c == 'A') {		// up arrow
					int len = get_history(shp, line, shd_backward);
					if (len > 0) {
						sh_reset_cursor();
						sh_clr_line();
						sh_printf("%s",line);
						p = line + len;
					}
					continue;
				}
				if(c == 'B') {		// down arrow
					int len = get_history(shp, line, shd_forward);

					if (len == 0)
						*line = 0;

					if(len >= 0) {
						sh_reset_cursor();
						sh_clr_line();
						sh_printf("%s", line);
						p = line + len;
					}
					continue;
				} // if (c=='B')
#endif //defined(SHELL_USE_HISTORY)
				if (c == 'D') {		// Left arrow
					if (p != line) {
						sh_cursor_left();
						--p;
					}
					continue;
				}
			} // if(bracket)
			continue;
		} // if(escape)
#endif  //#if defined(SHELL_USE_ESC_SEQ)

		if ((c == 8) || (c == 127)) {  //BS
			if (p != line) {
				sh_printf("%c%c%c", (char)8,0x20,(char)8);
				--p;
			}
			continue;
		}
		if (c == '\r') {  // Enter
			sh_printf(SH_NL_STR);
#if defined(SHELL_USE_HISTORY)
			if (!password_mode)
				save_history(shp, line, p - line);
#endif
			*p = 0;
			return 0;
		}
#if defined(SHELL_USE_COMPLETION)
		if(c == '\t') {			// TAB
			if(p < line + size - 1) {
				*p = 0;
				get_completions(line);
				int len = process_completions(line, p - line, size);
				if (len > 0) {
					write_completions(line, p - line);
					p = line + len;
				}
			}
		continue;
		}
#endif	//defined(SHELL_USE_COMPLETION)

#if defined(SHELL_USE_HISTORY)
		if(c == 14) { //down
			int len = get_history(shp, line, shd_forward);

			if(len == 0)
				*line = 0;

			if (len >= 0) {
				sh_reset_cursor();
				sh_clr_line();
				sh_printf("%s",line);
				p = line + len;
			}
			continue;
		}
		if(c == 16) { //up
			int len = get_history(shp, line, shd_backward);

			if (len > 0) {
				sh_reset_cursor();
				sh_clr_line();
				sh_printf("%s",line);
				p = line + len;
			}
			continue;
		}
#endif	//defined(SHELL_USE_HISTORY)
		if(c < 0x20)
			continue;

		if(p < line + size - 1) {
			if (!password_mode)
				sh_printf("%c",c);
			*p++ = (char)c;
		}
	}
}

static uint8_t cmd_exec(const shell_command_t *scp, char *name, int argc, char *argv[]) {
	while(scp->command_name) {
		if (strcmp(scp->command_name, name) == 0 && (!scp->only_for_root || (scp->only_for_root && super_user_mode))) {
			scp->function(argc, argv);
			return 0;
		}
		scp++;
	}
	return 1;
}

static void transmit_task( void *pvParameters ) {
	uint8_t ch;
	while(1){
		while(cq_get(&transmit_queue, &ch) == pdTRUE) {
			HAL_UART_Transmit(configuration->uart, &ch, 1, osWaitForever);
			xEventGroupSetBits(tx_full_event, EB_TRANSMIT_QUEUE_FULL);
		}
		osDelay(10);
	}
}

static void console_task( void *pvParameters ) {
	const shell_command_t *scp = configuration->commands;
	char *lp, *cmd, *tokp, line[SHELL_MAX_LINE_LENGTH];
	char *args[SHELL_MAX_ARGUMENTS];
	memset(line, 0, SHELL_MAX_LINE_LENGTH);

#if defined(SHELL_USE_HISTORY)
	*(history_buffer) = 0;
	shell_history_t hist = { .buffer = history_buffer,
							 .size = configuration->history_buffer_size,
							 .begin = 0,
							 .end = 0,
							 .cur = 0 };
	shell_history_t *shp = &hist;
#else
	shell_history_t *shp = NULL;
#endif

	sh_printf(SH_NL_STR);
	sh_printf(configuration->app_title ? configuration->app_title : APP_TITLE);
	sh_printf(SH_NL_STR);

	while(1) {
		prompt();
		if (sh_get_line(line, sizeof(line), shp) != 0)
			continue;

		if (password_mode) {
			super_user_mode = strcmp(line, SUPER_USER_PASSWORD) == 0;
			password_mode = 0;
			continue;
		}
		lp=parse_arguments(line, &tokp);
		cmd = lp;
		int n = 0;
		while((lp = parse_arguments(NULL, &tokp))) {
			if(n >= SHELL_MAX_ARGUMENTS) {
				sh_printf("Too much arguments"SH_NL_STR);
				cmd = NULL;
				break;
			}
			args[n++] = lp;
		}
		args[n] = NULL;
		if (cmd) {
			if (strcmp(cmd,"help") == 0) {
				if (n > 0) {
					sh_usage("help");
					continue;
				}
				sh_printf("Commands: help ");
				list_commands(SHELL_COMMANDS);
				if (scp)
					list_commands(scp);

				sh_printf(SH_NL_STR);
			}
			else if(cmd_exec(SHELL_COMMANDS, cmd, n, args) &&
					((scp == NULL) || cmd_exec(scp, cmd, n, args))) {
				sh_printf("%s", cmd);
				sh_printf(" ?"SH_NL_STR);
			}
		}
	}
}

/**
 * Initialize shell subsytem
 * \param cfg shell configuration parameters
 * \return pdTRUE if successfully or pdFAIL of soemthing went wrong
 */
BaseType_t sh_init(shell_configuration_t *cfg) {
	configuration = cfg;
	console_enabled = 1;
	password_mode = 0;
#if defined(DEBUG)
	super_user_mode = 1;
#else
	super_user_mode = 0;
#endif
    receive_queue=xQueueCreate(SHELL_QUEUE_LENGTH	, sizeof(uint8_t));
    configASSERT(receive_queue);

    cq_init(&transmit_queue,TRANSMIT_QUEUE_LENGTH);

    tx_full_event = xEventGroupCreate();
    configASSERT(tx_full_event);

    HAL_UART_Receive_IT(configuration->uart, &receive_buffer, 1U);

#if defined(SHELL_DEBUG) && defined(DATE_IN_LOG_MESSAGES)
    if (rtc)
    	sh_set_rtc(2016,11,3,2,13,42,0);
#endif

#if defined(SHELL_USE_HISTORY)
	if (cfg->history_buffer_size==0) {
		pr_err("shell history enabled but history size is 0");
		return pdFAIL;
	}
	history_buffer = pvPortMalloc(cfg->history_buffer_size);
	configASSERT(history_buffer);
#endif

#if defined(SHELL_USE_COMPLETION)
	completion=pvPortMalloc(cfg->history_buffer_size);
	configASSERT(completion);
#endif

	configASSERT(xTaskCreate(transmit_task, "CL tr", configuration->stack_size, NULL, configuration->priority, NULL)==pdPASS);
	configASSERT(xTaskCreate(console_task, "CLI", configuration->stack_size, NULL,  configuration->priority, NULL) == pdPASS);

	return pdPASS;
}

#if USE_STANDARD_PRINTF
PUTCHAR_PROTOTYPE
{
    if (shell.console_enabled)
         tr_queue(ch);

    return ch;
}
#endif


static void put_string(const char *string, uint16_t length) {
	for (int i = 0; i < length; ++i)
		tr_queue(*(string + i));
}

static inline void prompt() {
	if (!password_mode)
		put_string(repeat_active
					? SHELL_REPEAT_PROMPT_STR
					: super_user_mode
					  	  ? SHELL_ROOT_PROMPT_STR
					  	  : SHELL_USER_PROMPT_STR, sizeof(SHELL_USER_PROMPT_STR));
}

#if DATE_IN_LOG_MESSAGES
void shell_log_get_date_and_time() {
	if (!shell.rtc)
		return;

	RTC_DateTypeDef date;
	RTC_TimeTypeDef time;
	HAL_RTC_GetDate(shell.rtc, &date, FORMAT_BIN);
	HAL_RTC_GetTime(shell.rtc, &time, FORMAT_BIN);
	sh_printf("%04d.%02d.%02d %02d:%02d:%02d: ", date.Year+2000, date.Month, date.Date, time.Hours, time.Minutes, time.Seconds);
}

BaseType_t shell_set_rtc(uint16_t year,uint8_t month, uint8_t day, uint8_t week_day, uint8_t hour, uint8_t min, uint8_t sec) {
	if (!shell.rtc)
		return pdFAIL;

	RTC_DateTypeDef date;
	date.Year = year - 2000;
	date.Month = month;
	date.Date = day;
	date.WeekDay = week_day;

	if (HAL_RTC_SetDate(shell.rtc, &date, FORMAT_BIN) != HAL_OK)
		return pdFAIL;


	RTC_TimeTypeDef time;
	time.Hours = hour;
	time.Minutes = min;
	time.Seconds = sec;
	time.TimeFormat = RTC_HOURFORMAT_24;
	time.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	time.StoreOperation = RTC_STOREOPERATION_RESET;

	return  (HAL_RTC_SetTime(shell.rtc, &time, FORMAT_BIN)) == HAL_OK ? pdPASS : pdFAIL;
}
#endif


#if !defined(USE_STANDARD_PRINTF)

static char *long_to_string_with_divisor(char *p,
                                         long num,
                                         unsigned radix,
                                         long divisor) {
  int i;
  char *q;
  long l, ll;

  l = num;
  if (divisor == 0) {
    ll = num;
  } else {
    ll = divisor;
  }

  q = p + MAX_FILLER;
  do {
    i = (int)(l % radix);
    i += '0';
    if (i > '9')
      i += 'A' - '0' - 10;
    *--q = i;
    l /= radix;
  } while ((ll /= radix) != 0);

  i = (int)(p + MAX_FILLER - q);
  do
    *p++ = *q++;
  while (--i);

  return p;
}

static char *ch_ltoa(char *p, long num, unsigned radix) {

  return long_to_string_with_divisor(p, num, radix, 0);
}

#if defined(STPRINTF_USE_FLOAT)
static const long pow10[FLOAT_PRECISION] = {
    10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000
};

static char *ftoa(char *p, double num, unsigned long precision) {
	  long l;

	  if ((precision == 0) || (precision > FLOAT_PRECISION))
	    precision = FLOAT_PRECISION;
	  precision = pow10[precision - 1];

	  l = (long)num;
	  p = long_to_string_with_divisor(p, l, 10, 0);
	  *p++ = '.';
	  l = (long)((num - l) * precision);
	  return long_to_string_with_divisor(p, l, 10, precision / 10);
}

#endif

/**
 * @brief   System formatted output function.
 * @details This function implements a minimal @p vprintf()-like functionality
 *          with output on a @p BaseSequentialStream.
 *          The general parameters format is: %[-][width|*][.precision|*][l|L]p.
 *          The following parameter types (p) are supported:
 *          - <b>x</b> hexadecimal integer.
 *          - <b>X</b> hexadecimal long.
 *          - <b>o</b> octal integer.
 *          - <b>O</b> octal long.
 *          - <b>d</b> decimal signed integer.
 *          - <b>D</b> decimal signed long.
 *          - <b>u</b> decimal unsigned integer.
 *          - <b>U</b> decimal unsigned long.
 *          - <b>c</b> character.
 *          - <b>s</b> string.
 *          .
 *
 * @param[in] chp       pointer to a @p BaseSequentialStream implementing object
 * @param[in] fmt       formatting string
 * @param[in] ap        list of parameters
 * @return              The number of bytes that would have been
 *                      written to @p chp if no stream error occurs
 *
 * @api
 */
int sh_vprintf(const char *fmt, va_list ap) {
  char *p, *s, c, filler;
  int i, precision, width;
  int n = 0;
  bool is_long, left_align;
  long l;
#if defined(STPRINTF_USE_FLOAT)
  float f;
  char tmpbuf[2*MAX_FILLER + 1];
#else
  char tmpbuf[MAX_FILLER + 1];
#endif

  while (TRUE) {
    c = *fmt++;
    if (c == 0)
      return n;
    if (c != '%') {
      sh_putchar(c);
      n++;
      continue;
    }
    p = tmpbuf;
    s = tmpbuf;
    left_align = FALSE;
    if (*fmt == '-') {
      fmt++;
      left_align = TRUE;
    }
    filler = ' ';
    if (*fmt == '0') {
      fmt++;
      filler = '0';
    }
    width = 0;
    while (TRUE) {
      c = *fmt++;
      if (c >= '0' && c <= '9')
        c -= '0';
      else if (c == '*')
        c = va_arg(ap, int);
      else
        break;
      width = width * 10 + c;
    }
    precision = 0;
    if (c == '.') {
      while (TRUE) {
        c = *fmt++;
        if (c >= '0' && c <= '9')
          c -= '0';
        else if (c == '*')
          c = va_arg(ap, int);
        else
          break;
        precision *= 10;
        precision += c;
      }
    }
    /* Long modifier.*/
     if (c == 'l' || c == 'L') {
       is_long = TRUE;
       if (*fmt)
         c = *fmt++;
     }
     else
       is_long = (c >= 'A') && (c <= 'Z');

     /* Command decoding.*/
     switch (c) {
     case 'c':
       filler = ' ';
       *p++ = va_arg(ap, int);
       break;
     case 's':
       filler = ' ';
       if ((s = va_arg(ap, char *)) == 0)
         s = "(null)";
       if (precision == 0)
         precision = 32767;
       for (p = s; *p && (--precision >= 0); p++)
         ;
       break;
     case 'D':
     case 'd':
     case 'I':
     case 'i':
         if (is_long)
           l = va_arg(ap, long);
         else
           l = va_arg(ap, int);
         if (l < 0) {
           *p++ = '-';
           l = -l;
         }
         p = ch_ltoa(p, l, 10);
         break;
   #if defined(STPRINTF_USE_FLOAT)
       case 'f':
         f = (float) va_arg(ap, double);
         if (f < 0) {
           *p++ = '-';
           f = -f;
         }
         p = ftoa(p, f, precision);
         break;
   #endif
       case 'X':
       case 'x':
         c = 16;
         goto unsigned_common;
       case 'U':
       case 'u':
         c = 10;
         goto unsigned_common;
       case 'O':
       case 'o':
         c = 8;
unsigned_common:
               if (is_long)
                 l = va_arg(ap, unsigned long);
               else
                 l = va_arg(ap, unsigned int);
               p = ch_ltoa(p, l, c);
               break;
             default:
               *p++ = c;
               break;
             }// switch

     i = (int)(p - s);
     if ((width -= i) < 0)
        width = 0;

     if (left_align == FALSE)
         width = -width;

     if (width < 0) {
       if (*s == '-' && filler == '0') {
         sh_putchar((uint8_t)*s++);
         n++;
         i--;
       }

       do {
    	   sh_putchar((uint8_t)filler);
    	   n++;
       } while (++width != 0);
     }

     while (--i >= 0) {
        sh_putchar((uint8_t)*s++);
        n++;
     }

     while (width) {
        sh_putchar((uint8_t)filler);
        n++;
        width--;
     }
  }//while(TRUE)
}

/**
 * @brief   System formatted output function.
 * @details This function implements a minimal @p printf() like functionality
 *          with output on a @p BaseSequentialStream.
 *          The general parameters format is: %[-][width|*][.precision|*][l|L]p.
 *          The following parameter types (p) are supported:
 *          - <b>x</b> hexadecimal integer.
 *          - <b>X</b> hexadecimal long.
 *          - <b>o</b> octal integer.
 *          - <b>O</b> octal long.
 *          - <b>d</b> decimal signed integer.
 *          - <b>D</b> decimal signed long.
 *          - <b>u</b> decimal unsigned integer.
 *          - <b>U</b> decimal unsigned long.
 *          - <b>c</b> character.
 *          - <b>s</b> string.
 *          .
 *
 * @param[in] chp       pointer to a @p BaseSequentialStream implementing object
 * @param[in] fmt       formatting string
 *
 * @api
 */
int sh_printf(const char *fmt, ...) {
  va_list ap;
  int formatted_bytes;

  va_start(ap, fmt);
  formatted_bytes = sh_vprintf(fmt, ap);
  va_end(ap);

  return formatted_bytes;
}

/**
 * @brief   System formatted output function.
 * @details This function implements a minimal @p printf() like functionality
 *          with output on a @p BaseSequentialStream.
 *          The general parameters format is: %[-][width|*][.precision|*][l|L]p.
 *          The following parameter types (p) are supported:
 *          - <b>x</b> hexadecimal integer.
 *          - <b>X</b> hexadecimal long.
 *          - <b>o</b> octal integer.
 *          - <b>O</b> octal long.
 *          - <b>d</b> decimal signed integer.
 *          - <b>D</b> decimal signed long.
 *          - <b>u</b> decimal unsigned integer.
 *          - <b>U</b> decimal unsigned long.
 *          - <b>c</b> character.
 *          - <b>s</b> string.
 *          .
 *
 * @param[in] chp       pointer to a @p BaseSequentialStream implementing object
 * @param[in] fmt       formatting string
 *
 * @api
 */
int sh_log_printf(const char *fmt, ...) {
  va_list ap;
  int formatted_bytes;
  if (!console_enabled)
	  return 0;

  va_start(ap, fmt);
  formatted_bytes = sh_vprintf(fmt, ap);
  va_end(ap);

  return formatted_bytes;
}


/**
 * @brief   System formatted output function.
 * @details This function implements a minimal @p vprintf()-like functionality
 *          with output on a @p BaseSequentialStream.
 *          The general parameters format is: %[-][width|*][.precision|*][l|L]p.
 *          The following parameter types (p) are supported:
 *          - <b>x</b> hexadecimal integer.
 *          - <b>X</b> hexadecimal long.
 *          - <b>o</b> octal integer.
 *          - <b>O</b> octal long.
 *          - <b>d</b> decimal signed integer.
 *          - <b>D</b> decimal signed long.
 *          - <b>u</b> decimal unsigned integer.
 *          - <b>U</b> decimal unsigned long.
 *          - <b>c</b> character.
 *          - <b>s</b> string.
 *          .
 * @post    @p str is NUL-terminated, unless @p size is 0.
 *
 * @param[in] str       pointer to a buffer
 * @param[in] size      maximum size of the buffer
 * @param[in] fmt       formatting string
 * @return              The number of characters (excluding the
 *                      terminating NUL byte) that would have been
 *                      stored in @p str if there was room.
 *
 * @api
 */

int sh_snprintf(char *str, size_t size, const char *fmt, ...) {
  va_list ap;
  int retval;

  /* Performing the print operation using the common code.*/
  va_start(ap, fmt);
  retval = sh_vprintf(fmt, ap);
  va_end(ap);

  /* Return number of bytes that would have been written.*/
  return retval;
}
#endif // USE_STANDARD_PRINTF


static void print_as_hex(uint8_t *address, uint8_t len) {
	for(uint8_t i = 0; i < len;  ++i)
		sh_printf("%02x ", *(address+i));
}

static void print_as_char(uint8_t *address, uint8_t len) {
	for (uint8_t i = 0; i < len; ++i) {
		uint8_t c = *(address+i);
		sh_printf("%c", c < 0x20 ? '.' : c);
	}
}

static inline void print_address(uint32_t address) {
	sh_printf("%08x:", address);
}

void sh_dump_memory(uint8_t *address, uint16_t length) {
	do {
		uint16_t rl = length<DUMP_BYTES_PER_LINE ? length : DUMP_BYTES_PER_LINE;
		print_address((uint32_t)address);
		print_as_hex(address, rl);
		sh_printf(" ");
		print_as_char(address, rl);
		sh_printf(SH_NL_STR);
		address += rl;
		length -= rl;
	}while(length > 0);
}


/** \brief  System Reset
    The function initiates a system reset request to reset the MCU.
 */
static void cmd_system_reset()
{
  __DSB();                                                     /* Ensure all outstanding memory accesses included
                                                              buffered write are completed before reset */
  SCB->AIRCR  = ((0x5FA << SCB_AIRCR_VECTKEY_Pos)      |
                 (SCB->AIRCR & SCB_AIRCR_PRIGROUP_Msk) |
                 SCB_AIRCR_SYSRESETREQ_Msk);                   /* Keep priority group unchanged */
  __DSB();                                                     /* Ensure completion of memory access */
  while(1);                                                    /* wait until reset */
}

void cmd_dump_memory(int argc, char *argv[]) {
	if (argc == 0) {
		sh_printf("usage: dump <address(hex)> [length=16]"SH_NL_STR);
		return;
	}
	char *endptr;
	uint32_t mem = strtol(argv[0], &endptr, 16);
	uint8_t *address = (uint8_t *)mem;
	uint16_t length = argc==1 ? DEFAULT_DUMP_LENGTH : atoi(argv[1]);
	sh_dump_memory(address, length);
}

/**
 * repeat command
 */
void cmd_repeat(int argc, char *argv[]) {
	if (repeat_active) {
		repeat_active = 0;
		return;
	}
	repeat_counter = 1;
	repeat_active = 1;
}

static void log_state_msg() {
	put_string(LOG_MSG, strlen(LOG_MSG));
	put_string(console_enabled ? ON_STR : OFF_STR,
			   console_enabled ? strlen(ON_STR) : strlen(OFF_STR));
	put_string(SH_NL_STR, strlen(SH_NL_STR));
}

void sh_toggle_log() {
	console_enabled ^= 1;
	log_state_msg();
}

void sh_log_off() {
	console_enabled = 0;
	log_state_msg();
}

void sh_log_on() {
	console_enabled = 1;
	log_state_msg();
}

static void cmd_log(int argc, char *argv[]) {
	if(argc == 1)
		console_enabled = strcmp(argv[0], ON_STR) == 0 ? 1 : 0;
	log_state_msg();
}

static void cmd_su(int argc, char *argv[]) {
	if (super_user_mode)
		return;

	sh_printf("password:");
	password_mode = 1;
}

static void cmd_logout(int argc, char *argv[]) {
	if (!super_user_mode)
		return;

	super_user_mode = 0;
	sh_printf("you are logged out"SH_NL_STR);
}

