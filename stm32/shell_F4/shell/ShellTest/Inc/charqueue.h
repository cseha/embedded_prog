/**
 * \file charqueue.h
 *
 * Created Nov 5, 2016
 *
 * \author zamek
*/
#ifndef CHARQUEUE_H_
#define CHARQUEUE_H_

#include <stdint.h>
#include <FreeRTOS.h>
#include <cmsis_os.h>

#ifdef __cplusplus
	extern "CharQueue" {
#endif

/**
 * \defgroup CHAR_QUEUE
 * @{
 */

/**
 * \addtogroup CHAR_QUEUE
 * @{
 */

/**
 * Simple character queue thread safe implementation.
 */
typedef struct {
	uint8_t *buffer;
	uint16_t w_ptr;
	uint16_t r_ptr;
	uint16_t size;
	uint16_t cnt;
}char_queue_t;

/**
 * Initialize charqueue
 *
 * \param queue pointer to the charqueue to initialize
 * \param size size of the charqueue
 * \return pdTRUE if successfully or pdFAIL
 */
BaseType_t cq_init(char_queue_t *queue, uint16_t size);

/**
 * Put a character to the queue
 *
 * This function is waits if the queue is full
 *
 * \param queue pointer to the queue
 * \param c character to put
 * \return pdTRUE if successfully or pdFAIL
 */
BaseType_t cq_put(char_queue_t *queue, uint8_t c);

/**
 * Get a character from the queue
 *
 * The function waits if the queue is empty
 * \param queue pointer to the queue
 * \param address of a character to store
 * \return pdTRUE if successfully or pdFAIL
 */
BaseType_t cq_get(char_queue_t *queue, uint8_t *c);

/**
 * Checks whether the queue is full
 *
 * \param queue pointer to queue to check
 * \return 1 if the queue is full or 0 if not
 */
uint8_t cq_is_full(char_queue_t *queue);

/**
 * @}
 */
/**
 * @}
 */

#ifdef __cplusplus
	}
#endif

#endif /* CHARQUEUE_H_ */
