#ifndef LINKED_H_INCLUDED
#define LINKED_H_INCLUDED

typedef enum {
	ON = 1,
	OFF = 0
}SW_DIRECTION;

typedef struct {
	int ID;
	int count;
	SW_DIRECTION direction;
}bounces_t;

typedef struct {
	bounces_t data;
	struct bouncer_stat_t *next;
} bounces_stat_t;

typedef struct {
    bounces_stat_t *head;
    int size;
} queue_head_t;

bounces_stat_t *createBounceListWithFirstData(queue_head_t *list, bounces_t data);

void addBounceToList(queue_head_t *list, bounces_t data);

int getBounceByID(int ID);

float getBounceMean(queue_head_t *list);

float getBounceVariance(int deviation);

float getBounceDeviation(int mean, queue_head_t *list);

void printBounces(queue_head_t *head);

#endif // LINKED_H_INCLUDED
