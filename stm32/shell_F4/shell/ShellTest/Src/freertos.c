/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include <stm32f4xx_hal.h>
#include <stdbool.h>
#include <string.h>

#include "cmsis_os.h"
#include "shell.h"
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId defaultTaskHandle;

/* USER CODE BEGIN Variables */
#define MIN_VAL 50
#define MAX_VAL 500
#define CMD_ARG_SET "set"
#define CMD_ARG_GET "get"
#define CMD_ARG_INIT "init"
#define CMD_ARG_DEINIT "deinit"
#define TASK_DELAY_MS 1000

static struct{
	bool initialized;
	uint16_t value;
} counter_control;
/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
void cmd_counter(int argc, char **argv);

void counter_task(const void *args);

BaseType_t counter_init();

BaseType_t counter_deinit();
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */
#define COUNTER_CMD "counter"

#define COUNTER_COMMANDS \
	{	.command_name = COUNTER_CMD, .function = cmd_counter }

#define APPLICATION_TITLE "===== ShellTest Demo ====="

extern UART_HandleTypeDef huart2;

static shell_command_t shell_commands[] ={
		COUNTER_COMMANDS,
		{NULL, NULL}
};

static shell_configuration_t shell_config = {
		.uart=&huart2,
		.commands=shell_commands,
		.stack_size=1024,
		.priority=osPriorityNormal,
		.rtc = NULL,
		.history_buffer_size=128,
		.app_title=APPLICATION_TITLE
};
/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	sh_init(&shell_config);
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
	counter_task(NULL);
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Application */
BaseType_t counter_init(){
	counter_control.initialized = true;
	counter_control.value = MIN_VAL;
	return pdTRUE;
}

BaseType_t counter_deinit(){
	counter_control.initialized = false;
	return pdTRUE;
}

void counter_task(const void *args){
	for(;;){
		counter_control.value = counter_control.value <= MAX_VAL
				? counter_control.value + 1
						: MIN_VAL;

		osDelay(TASK_DELAY_MS);
	}
}

static void cmd_usage(){
	sh_printf("usage: %s [set <value> | get | init | deinit ]\r\n", COUNTER_CMD);
}
void cmd_counter(int argc, char **argv){
	if(strcasecmp(argv[0], CMD_ARG_INIT) == 0){
		counter_init();
		sh_printf("counter is initialized\r\n");
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_DEINIT) == 0){
		counter_deinit();
		sh_printf("counter is not initialised\r\n");
		return;
	}

	if(!counter_control.initialized){
		sh_printf("counter is not initialised, type init to initalize\r\n");
		return;
	}

	if(argc == 0){
		cmd_usage();
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_SET) == 0){
		if(argc != 2){
			cmd_usage();
			return;
		}

		uint16_t nv = atoi(argv[1]);
		if(nv >= MIN_VAL && nv <= MAX_VAL){
			counter_control.value = nv;
			sh_printf("value set to %d\r\n", nv);
			return;
		}

		sh_printf("value must be between %d and %d\r\n", MIN_VAL, MAX_VAL);
		return;
	}

	if(strcasecmp(argv[0], CMD_ARG_GET) == 0){
		sh_printf("value is %d\r\n", counter_control.value);
		return;
	}

	sh_printf("unknown command: %s\r\n", argv[0]);
	cmd_usage();
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
